package com.avon.repNarFronterasAbiertas;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.util.DefaultTempFileCreationStrategy;
import org.apache.poi.util.TempFile;
import org.apache.poi.xssf.streaming.SXSSFRow;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class Prueba2 {

	public static void main(String[] args) {
		Prueba2 prueba2 = new Prueba2();
		prueba2.generarExcel();
	}
	
	public void generarExcel() {
		File dir = new File(System.getProperty("java.io.tmpdir"),"poifiles");
    	dir.mkdir();
    	//TempFile.setTempFileCreationStrategy(new TempFile.DefaultTempFileCreationStrategy(dir));
    	TempFile.setTempFileCreationStrategy(new DefaultTempFileCreationStrategy(dir));
		SXSSFWorkbook wb = null;
	
		FileWriter writer = null;
		// Row and column indexes
        int idx = 0;
        int idy = 0;
        
        try {
        	
        	
        	writer = new FileWriter("/Apps/scheduler/Apps/OTSTools/fronterasAbiertasV2/p.txt");
        	writer.write("1\n");
		//wb = new SXSSFWorkbook();
        	wb = new SXSSFWorkbook(100);
		
		
		
		
    	
		writer.write("2\n");
		 
        Cell c = null;

        //Cell style for header row
        CellStyle cs = wb.createCellStyle();
        writer.write("3\n");
        cs.setFillForegroundColor(IndexedColors.LIME.getIndex());
        writer.write("4\n");
        //cs.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
        Font f = wb.createFont();
        writer.write("5\n");
        //f.setBoldweight(Font.COLOR_NORMAL);
        f.setFontHeightInPoints((short) 12);
        writer.write("6\n");
        cs.setFont(f);
        writer.write("7\n");

        //New Sheet
        SXSSFSheet sheet1 = null;
        writer.write("8\n");
        sheet1 = wb.createSheet("myData");
        writer.write("9\n");
        SXSSFRow row = sheet1.createRow(idx);
        writer.write("10\n");
        c = row.createCell(idy);
        writer.write("11\n");
        c.setCellValue("Columna 1");
        writer.write("12\n");
        c.setCellStyle(cs);
        writer.write("13\n");
        for (int i=0;i<5;i++) {
        	idx++;
        	row = sheet1.createRow(idx);
        	writer.write("14\n");
        	c = row.createCell(0);
        	writer.write("15\n");
        	c.setCellValue(i);
        	writer.write("16\n");
        }
        
			//FileOutputStream fileOut = new FileOutputStream("c:/jtb/uno.xlsx" );
        	//FileOutputStream fileOut = new FileOutputStream("/Apps/ftpdir/fronterasAbiertas/uno.xlsx" );
            FileOutputStream fileOut = new FileOutputStream("/Apps/scheduler/Apps/OTSTools/fronterasAbiertasV2/poifiles/uno.xlsx");
        	writer.write("17\n");
			wb.write(fileOut);
			writer.write("18\n");
            fileOut.close();
            writer.write("19\n");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			StringWriter sw=new StringWriter();
			PrintWriter pw=new PrintWriter(sw);
			e.printStackTrace(pw);
			String sStackTrace=sw.toString();
			try {
				writer.write(sStackTrace);
				
				if ( writer != null) {
				writer.close();
				}
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
        finally {
        	if ( writer != null) {
				try {
					writer.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				}
        }
	}
}
