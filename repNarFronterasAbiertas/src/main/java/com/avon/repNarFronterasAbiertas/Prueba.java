package com.avon.repNarFronterasAbiertas;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

public class Prueba {

	private String directorioArchivoPropiedades = "c:/Apps/repNarFronterasAbiertas/repNarFronterasAbiertas/config.properties";
	
    private Properties propiedades;
    
	public static void main(String args[]) {
		
		Prueba prueba = new Prueba();
	    String valor = prueba.recuperarPropiedad("dirArchivoExcel");
	    System.out.println(valor);
	    System.out.println(String.format("%05d", 5));
	    String pattern = "ddMMyyyy";
	    SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
	    String date = simpleDateFormat.format(new Date());
	    System.out.println(date);
	    
	    String result = "";
		InputStream inputStream = null;
		
		try {
			Properties prop = new Properties();
			String propFileName = "config.properties";
 
			inputStream = Prueba.class.getClassLoader().getResourceAsStream(propFileName);
 
			if (inputStream != null) {
				prop.load(inputStream);
			} else {
				throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
			}
 
			Date time = new Date(System.currentTimeMillis());
 
			// get the property value and print it out
			String user = prop.getProperty("dirArchivoExcel");
			String company1 = prop.getProperty("nomArchivoExcel");

 
			result = "Company List = " + company1 ;
			System.out.println(result + "\nProgram Ran on " + time + " by user=" + user);
		} catch (Exception e) {
			System.out.println("Exception: " + e);
		} finally {
			try {
				inputStream.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	    
	    
	    
		/*
	    File jarPath=new File(Prueba.class.getProtectionDomain().getCodeSource().getLocation().getPath());
        String propertiesPath=jarPath.getParentFile().getAbsolutePath();
        System.out.println(" propertiesPath-"+propertiesPath);
        prueba.propiedades = new Properties();
        try {
			prueba.propiedades.load(new FileInputStream(propertiesPath+"/config.properties"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		*/
	    
	}
	
	private void cargarPropiedades() {
    	InputStream isrParams = null;
    	propiedades = new Properties();
    	try {
    		isrParams = new FileInputStream(directorioArchivoPropiedades);
			propiedades.load(isrParams);
		} catch (IOException e) {
			e.printStackTrace();
		}
    }
	
	/*
	private void cargarPropiedades() {
		File jarPath=new File(Prueba.class.getProtectionDomain().getCodeSource().getLocation().getPath());
        String propertiesPath=jarPath.getParentFile().getAbsolutePath();
        System.out.println(" propertiesPath-"+propertiesPath);
        propiedades = new Properties();
    	//InputStream isrParams = null;
    	try {
    		//isrParams = new FileInputStream(propertiesPath+"/config.properties");
			propiedades.load(new FileInputStream(propertiesPath+"/config.properties"));
		} catch (IOException e) {
			e.printStackTrace();
		}
    }
    */
	
    public String recuperarPropiedad(String key) {
    	String valorPropiedad = "";
    	
    	cargarPropiedades();
    	valorPropiedad = propiedades.getProperty(key);
    	return valorPropiedad;
    }
}
