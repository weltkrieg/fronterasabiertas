package com.avon.repNarFronterasAbiertas.sql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DatabaseConnection {

	private static DatabaseConnection connectionInstance = null;
	
	public static Connection getConnection() {
	    if (connectionInstance == null)
	      connectionInstance = new DatabaseConnection(); 
	    return connectionInstance.createConnection();
	  }
	  
	  private Connection createConnection() {
	    Connection conexion = null;
	    try {
	      Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
	      conexion = DriverManager.getConnection("jdbc:sqlserver://134.65.155.187:1433;databaseName=internet;", "aplsos", "Int3rs0s");
	    } catch (SQLException sqlException) {
	      sqlException.printStackTrace();
	      //loggerError.error("Hubo un problema al intentar conectarse con la base de datos " + Parameters.getProperty("db.url") + sqlException);
	      //loggerMail.error("ERROR AL CONECTARSE A LAS BASE DE DATOS DE LISTA NAR: " + sqlException.getMessage());
	      return null;
	    } catch (ClassNotFoundException ex) {
	      ex.printStackTrace();
	      //loggerError.error("El driver de la Base de datos no se encontro: " + ex);
	      //loggerMail.error("ERROR AL CONECTARSE A LAS BASE DE DATOS DE LISTA NAR: " + ex.getMessage());
	      return null;
	    } 
	    return conexion;
	  }
}
