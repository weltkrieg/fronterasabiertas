package com.avon.repNarFronterasAbiertas.sql;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;


public class Query {

	private Connection conn = null;
	  
	  private PreparedStatement stmt = null;
	  
	  private ResultSet rs = null;
	  
	  private HashMap<String, Integer> medatada = null;
	  
	  public Query() {
	    this.conn = DatabaseConnection.getConnection();
	  }
	  
	  public void createQuery(String query, ArrayList<Object> valuesList) {
	    this.stmt = null;
	    try {
	      if (this.conn != null) {
	        this.stmt = this.conn.prepareStatement(query);
	        if (valuesList != null)
	          setValuesPreparedStatement(this.stmt, valuesList); 
	      } 
	    } catch (SQLException sqlException) {
	      
	      System.err.println("Error Query [" + getQuery(query, valuesList) + "]");
	      sqlException.printStackTrace();
	    } catch (FileNotFoundException FileException) {
	    	System.out.println("");
	      //loggerError.error("Error al crear el query: " + FileException);
	      FileException.printStackTrace();
	    } 
	  }
	  
	  private void setValuesPreparedStatement(PreparedStatement preparedStatement, ArrayList<Object> valuesList) throws SQLException, FileNotFoundException {
	    for (int i = 0; i < valuesList.size(); i++) {
	      if (valuesList.get(i) instanceof Integer) {
	        preparedStatement.setInt(i + 1, ((Integer)valuesList.get(i)).intValue());
	      } else if (valuesList.get(i) instanceof Float) {
	        preparedStatement.setFloat(i + 1, ((Float)valuesList.get(i)).floatValue());
	      } else if (valuesList.get(i) instanceof String) {
	        preparedStatement.setString(i + 1, (String)valuesList.get(i));
	      } else if (valuesList.get(i) instanceof Double) {
	        preparedStatement.setDouble(i + 1, ((Double)valuesList.get(i)).doubleValue());
	      } else if (valuesList.get(i) instanceof Date) {
	        preparedStatement.setDate(i + 1, (Date)valuesList.get(i));
	      } else if (valuesList.get(i) instanceof File) {
	        File imagen = (File)valuesList.get(i);
	        long length = imagen.length();
	        InputStream imagenStream = new FileInputStream(imagen);
	        preparedStatement.setBinaryStream(i + 1, imagenStream, (int)length);
	      } else if (valuesList.get(i) instanceof Long) {
	        preparedStatement.setLong(i + 1, ((Long)valuesList.get(i)).longValue());
	      } else if (valuesList.get(i) instanceof Timestamp) {
	        preparedStatement.setTimestamp(i + 1, (Timestamp)valuesList.get(i));
	      } else if (valuesList.get(i) == null) {
	        preparedStatement.setString(i + 1, (String)null);
	      } 
	    } 
	  }
	  
	  public boolean executeSelect() {
	    this.rs = null;
	    this.medatada = null;
	    try {
	      this.rs = this.stmt.executeQuery();
	      ResultSetMetaData meta = this.rs.getMetaData();
	      int count = meta.getColumnCount();
	      this.medatada = new HashMap<>(count);
	      for (int i = 1; i <= count; i++)
	        this.medatada.put(meta.getColumnName(i), new Integer(i)); 
	    } catch (SQLException e) {
	      //loggerError.error("Error al ejecutar el query: " + e);
	      e.printStackTrace();
	      return false;
	    } 
	    return true;
	  }
	  
	  public int executeUpdate() {
	    int count = 0;
	    try {
	      count = this.stmt.executeUpdate();
	    } catch (SQLException e) {
	      //loggerError.error("Error al ejecutar el query: " + e);
	      e.printStackTrace();
	    } 
	    return count;
	  }
	  
	  public ResultSet getResultSet() {
	    return this.rs;
	  }
	  
	  public PreparedStatement getPreparedStatement() {
	    return this.stmt;
	  }
	  
	  public Connection getConnection() {
	    return this.conn;
	  }
	  
	  public HashMap<String, Integer> getMetaData() {
	    return this.medatada;
	  }
	  
	  public void closeQuey() {
	    try {
	      if (this.rs != null) {
	        this.rs.close();
	        this.rs = null;
	      } 
	      if (this.stmt != null) {
	        this.stmt.close();
	        this.stmt = null;
	      } 
	      if (this.conn != null) {
	        this.conn.close();
	        this.conn = null;
	      } 
	    } catch (SQLException sqlException) {
	      System.err.println("Error al cerrar las conexiones [" + sqlException + "]");
	    } 
	  }
	  
	  public String getQuery(String query, ArrayList<Object> valuesList) {
	    StringBuffer sbsql = new StringBuffer(query);
	    int indice = 0;
	    if (valuesList != null)
	      for (int i = 0; i < valuesList.size(); i++) {
	        indice = sbsql.indexOf("?");
	        sbsql.delete(indice, indice + 1);
	        if (valuesList.get(i) instanceof Integer) {
	          sbsql.insert(indice, valuesList.get(i));
	        } else if (valuesList.get(i) instanceof Float) {
	          sbsql.insert(indice, valuesList.get(i));
	        } else if (valuesList.get(i) instanceof String) {
	          String tmp = "'" + (String)valuesList.get(i) + "'";
	          sbsql.insert(indice, tmp);
	        } else if (valuesList.get(i) instanceof Double) {
	          sbsql.insert(indice, valuesList.get(i));
	        } else if (valuesList.get(i) instanceof Date) {
	          String tmp = "'" + (Date)valuesList.get(i) + "'";
	          sbsql.insert(indice, tmp);
	        } else if (valuesList.get(i) instanceof File) {
	          sbsql.insert(indice, valuesList.get(i));
	        } else if (valuesList.get(i) instanceof Long) {
	          sbsql.insert(indice, valuesList.get(i));
	        } else if (valuesList.get(i) instanceof Timestamp) {
	          String tmp = "'" + (Timestamp)valuesList.get(i) + "'";
	          sbsql.insert(indice, tmp);
	        } else if (valuesList.get(i) == null) {
	          sbsql.insert(indice, "null");
	        } 
	      }  
	    return sbsql.toString();
	  }
}
