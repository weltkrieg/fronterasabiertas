package com.avon.repNarFronterasAbiertas.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import com.avon.repNarFronterasAbiertas.dto.OrdenBean;
import com.avon.repNarFronterasAbiertas.dto.OrdenRedBean;
import com.avon.repNarFronterasAbiertas.dto.OrdenZonaBean;
import com.avon.repNarFronterasAbiertas.dto.ZonasBean;
import com.avon.repNarFronterasAbiertas.sql.Query;


public abstract class ReportesDao {

	public static List<ZonasBean> obtenerZonas(int anio) {
	    ArrayList<ZonasBean> listZonas = new ArrayList<>();
	    Query query = new Query();
	    ResultSet rs = null;
	    ArrayList<Object> valores = new ArrayList();
	    valores.add(Integer.valueOf(anio));
	    query.createQuery("SELECT P.ZONE, MAX(P.CAMP_YEAR) AS ANIO FROM PAYMENT P WHERE P.CAMP_YEAR >= ? GROUP BY P.ZONE ORDER BY P.ZONE", valores);
	    if (query.executeSelect()) {
	      rs = query.getResultSet();
	      try {
	        while (rs.next()) {
	          ZonasBean beanZona = new ZonasBean();
	          beanZona.setZona(rs.getInt("ZONE"));
	          beanZona.setAnio(rs.getInt("ANIO"));
	          listZonas.add(beanZona);
	        } 
	      } catch (SQLException e) {
	        e.printStackTrace();
	        //loggerError.error("Error en query" + e);
	      } finally {
	        query.closeQuey();
	        query = null;
	      } 
	    } else {
	      //loggerError.error("Error en query: " + query.getQuery("SELECT P.ZONE, MAX(P.CAMP_YEAR) AS ANIO FROM PAYMENT P WHERE P.CAMP_YEAR >= ? GROUP BY P.ZONE ORDER BY P.ZONE", valores));
	    } 
	    return listZonas;
	  }
	  
	  public static int obtenerCampaniaActual() {
	    Calendar fecha = Calendar.getInstance();
	    int anio = fecha.get(1);
	    Query query = new Query();
	    ResultSet rs = null;
	    int campaign = 0;
	    ArrayList<Object> valores = new ArrayList();
	    valores.add(Integer.valueOf(anio));
	    query.createQuery("SELECT MAX(P.CAMPAIGN) AS CAMPAIGN FROM PAYMENT P WHERE P.CAMP_YEAR >= ? ", valores);
	    if (query.executeSelect()) {
	      rs = query.getResultSet();
	      try {
	        if (rs.next())
	          campaign = rs.getInt("CAMPAIGN"); 
	      } catch (SQLException e) {
	        e.printStackTrace();
	        //loggerError.error("Error en query" + e);
	      } finally {
	        query.closeQuey();
	        query = null;
	      } 
	    } else {
	      //loggerError.error("Error en query: " + query.getQuery("SELECT MAX(P.CAMPAIGN) AS CAMPAIGN FROM PAYMENT P WHERE P.CAMP_YEAR >= ? ", valores));
	    } 
	    return campaign;
	  }
	  
	  public static int obtenerCampania(int anio, int zona) {
	    Query query = new Query();
	    ResultSet rs = null;
	    int campaign = 0;
	    ArrayList<Object> valores = new ArrayList();
	    valores.add(Integer.valueOf(anio));
	    valores.add(Integer.valueOf(zona));
	    query.createQuery("SELECT MAX(P.CAMPAIGN) AS CAMPAIGN FROM PAYMENT P WHERE P.CAMP_YEAR = ? AND P.ZONE = ? ", valores);
	    if (query.executeSelect()) {
	      rs = query.getResultSet();
	      try {
	        if (rs.next())
	          campaign = rs.getInt("CAMPAIGN"); 
	      } catch (SQLException e) {
	        e.printStackTrace();
	        //loggerError.error("Error en query" + e);
	      } finally {
	        query.closeQuey();
	        query = null;
	      } 
	    } else {
	      //loggerError.error("Error en query: " + query.getQuery("SELECT MAX(P.CAMPAIGN) AS CAMPAIGN FROM PAYMENT P WHERE P.CAMP_YEAR = ? AND P.ZONE = ? ", valores));
	    } 
	    return campaign;
	  }
	  
	  public static int obtenerCantidadTotalOrdenes(int anio, int campania, int zona) {
	    Query query = new Query();
	    ResultSet rs = null;
	    int totOrd = 0;
	    ArrayList<Object> valores = new ArrayList();
	    valores.add(Integer.valueOf(anio));
	    valores.add(Integer.valueOf(campania));
	    valores.add(Integer.valueOf(zona));
	    query.createQuery("SELECT COUNT(P.ACCOUNT) AS TOT_ORDENES FROM PAYMENT P LEFT OUTER JOIN PAYMENTT T ON P.ACCOUNT=T.ACCOUNT AND P.CAMP_YEAR=T.CAMP_YEAR AND P.CAMPAIGN=T.CAMPAIGN WHERE P.CAMP_YEAR= ? AND P.CAMPAIGN= ? AND P.ZONE= ?", valores);
	    if (query.executeSelect()) {
	      rs = query.getResultSet();
	      try {
	        if (rs.next())
	          totOrd = rs.getInt("TOT_ORDENES"); 
	      } catch (SQLException e) {
	        e.printStackTrace();
	        //loggerError.error("Error en query" + e);
	      } finally {
	        query.closeQuey();
	        query = null;
	      } 
	    } else {
	      //loggerError.error("Error en query: " + query.getQuery("SELECT COUNT(P.ACCOUNT) AS TOT_ORDENES FROM PAYMENT P LEFT OUTER JOIN PAYMENTT T ON P.ACCOUNT=T.ACCOUNT AND P.CAMP_YEAR=T.CAMP_YEAR AND P.CAMPAIGN=T.CAMPAIGN WHERE P.CAMP_YEAR= ? AND P.CAMPAIGN= ? AND P.ZONE= ?", valores));
	    } 
	    return totOrd;
	  }
	  
	  public static int obtenerCantidadOrdenesDevolucion(int anio, int campania, int zona) {
	    Query query = new Query();
	    ResultSet rs = null;
	    int totOrdDevolucion = 0;
	    ArrayList<Object> valores = new ArrayList();
	    valores.add(Integer.valueOf(anio));
	    valores.add(Integer.valueOf(campania));
	    valores.add(Integer.valueOf(zona));
	    query.createQuery("SELECT COUNT(P.ACCOUNT) AS TOTAL FROM PAYMENT P LEFT OUTER JOIN PAYMENTT T ON P.ACCOUNT=T.ACCOUNT AND P.CAMP_YEAR=T.CAMP_YEAR AND P.CAMPAIGN=T.CAMPAIGN WHERE T.DEVOL_RSN IS NULL AND T.ROUND_NUM IS NULL AND T.HJK_TYP IS NULL AND P.CAMP_YEAR = ? AND P.CAMPAIGN = ? AND P.ZONE = ? ", valores);
	    if (query.executeSelect()) {
	      rs = query.getResultSet();
	      try {
	        if (rs.next())
	          totOrdDevolucion = rs.getInt("TOTAL"); 
	      } catch (SQLException e) {
	        e.printStackTrace();
	        //loggerError.error("Error en query" + e);
	      } finally {
	        query.closeQuey();
	        query = null;
	      } 
	    } else {
	      //loggerError.error("Error en query: " + query.getQuery("SELECT COUNT(P.ACCOUNT) AS TOTAL FROM PAYMENT P LEFT OUTER JOIN PAYMENTT T ON P.ACCOUNT=T.ACCOUNT AND P.CAMP_YEAR=T.CAMP_YEAR AND P.CAMPAIGN=T.CAMPAIGN WHERE T.DEVOL_RSN IS NULL AND T.ROUND_NUM IS NULL AND T.HJK_TYP IS NULL AND P.CAMP_YEAR = ? AND P.CAMPAIGN = ? AND P.ZONE = ? ", valores));
	    } 
	    return totOrdDevolucion;
	  }
	  
	  public static float obtenerSumaTotalOrdenesDevolucion(int anio, int campania, int zona) {
	    Query query = new Query();
	    ResultSet rs = null;
	    float total = 0.0F;
	    ArrayList<Object> valores = new ArrayList();
	    valores.add(Integer.valueOf(anio));
	    valores.add(Integer.valueOf(campania));
	    valores.add(Integer.valueOf(zona));
	    query.createQuery("SELECT SUM(P.SLIP_VALUE) AS TOTAL FROM PAYMENT P LEFT OUTER JOIN PAYMENTT T ON P.ACCOUNT=T.ACCOUNT AND P.CAMP_YEAR=T.CAMP_YEAR AND P.CAMPAIGN=T.CAMPAIGN WHERE T.DEVOL_RSN IS NULL AND T.ROUND_NUM IS NULL AND T.HJK_TYP IS NULL AND P.CAMP_YEAR = ? AND P.CAMPAIGN = ? AND P.ZONE= ? ", valores);
	    if (query.executeSelect()) {
	      rs = query.getResultSet();
	      try {
	        if (rs.next())
	          total = rs.getFloat("TOTAL"); 
	      } catch (SQLException e) {
	        e.printStackTrace();
	        //loggerError.error("Error en query" + e);
	        return 0.0F;
	      } finally {
	        query.closeQuey();
	        query = null;
	      } 
	    } else {
	      //loggerError.error("Error en query: " + query.getQuery("SELECT COUNT(P.ACCOUNT) AS TOTAL FROM PAYMENT P LEFT OUTER JOIN PAYMENTT T ON P.ACCOUNT=T.ACCOUNT AND P.CAMP_YEAR=T.CAMP_YEAR AND P.CAMPAIGN=T.CAMPAIGN WHERE T.DEVOL_RSN IS NULL AND T.ROUND_NUM IS NULL AND T.HJK_TYP IS NULL AND P.CAMP_YEAR = ? AND P.CAMPAIGN = ? AND P.ZONE = ? ", valores));
	    } 
	    return total;
	  }
	  
	  public static List<OrdenBean> obtenerOrdenesDevolucion(int anio, int campania, int zona) {
	    List<OrdenBean> listOrdenesDev = new ArrayList<>();
	    Query query = new Query();
	    ResultSet rs = null;
	    ArrayList<Object> valores = new ArrayList();
	    valores.add(Integer.valueOf(anio));
	    valores.add(Integer.valueOf(campania));
	    valores.add(Integer.valueOf(zona));
	    query.createQuery("SELECT R.SECTION, P.ACCOUNT, R.REP_NAME,R.ADDRESS1, R.ADDRESS2, R.ADDRESS3, ISNULL(P.MODIF_VAL,P.COD_VALUE) COD_VALUE, P.SLIP_VALUE, P.CTN_COUNT, ISNULL(T.DELIVERY1ST +' - '+ S.RSN_DESC,'') AS DVL_RSN, BALANCE, P.BLOCKED_STATUS,T.ROUND_NUM,T.DELIVERY1ST,T.DEVOL_RSN FROM PAYMENT P INNER JOIN REPRESENTATIVES R ON P.ACCOUNT = R.ACCOUNT LEFT OUTER JOIN PAYMENTT T ON P.ACCOUNT=T.ACCOUNT AND P.CAMP_YEAR=T.CAMP_YEAR AND P.CAMPAIGN=T.CAMPAIGN LEFT OUTER JOIN DEVOL_RSN S ON T.DELIVERY1ST = S.DEVOL_RSN WHERE P.CAMP_YEAR = ? AND  P.CAMPAIGN = ? AND P.ZONE = ? AND T.HJK_TYP IS NULL ORDER BY R.SECTION ASC, P.SLIP_VALUE DESC ", valores);
	    if (query.executeSelect()) {
	      rs = query.getResultSet();
	      try {
	        while (rs.next()) {
	          OrdenBean bean = new OrdenBean();
	          bean.setRegistro(Integer.valueOf(rs.getInt("ACCOUNT")));
	          bean.setNombre(rs.getString("REP_NAME"));
	          bean.setDireccion(rs.getString("ADDRESS1"));
	          bean.setCalles(rs.getString("ADDRESS2"));
	          bean.setPoblacion(rs.getString("ADDRESS3"));
	          bean.setValorCod(rs.getFloat("COD_VALUE"));
	          bean.setValorPedido(rs.getFloat("SLIP_VALUE"));
	          bean.setRed(rs.getInt("SECTION"));
	          bean.setCajas(Integer.valueOf(rs.getInt("CTN_COUNT")));
	          bean.setCausa(rs.getString("DVL_RSN"));
	          bean.setBalance(rs.getFloat("BALANCE"));
	          bean.setBlockedStatus(Integer.valueOf(rs.getInt("BLOCKED_STATUS")));
	          bean.setRoundNum(rs.getString("ROUND_NUM"));
	          bean.setDelivery1St(rs.getString("DELIVERY1ST"));
	          bean.setDevolRsn(rs.getString("DEVOL_RSN"));
	          listOrdenesDev.add(bean);
	        } 
	      } catch (SQLException e) {
	        e.printStackTrace();
	        //loggerError.error("Error en query" + e);
	      } finally {
	        query.closeQuey();
	        query = null;
	      } 
	    } else {
	      //loggerError.error("Error en query: " + query.getQuery("SELECT R.SECTION, P.ACCOUNT, R.REP_NAME,R.ADDRESS1, R.ADDRESS2, R.ADDRESS3, ISNULL(P.MODIF_VAL,P.COD_VALUE) COD_VALUE, P.SLIP_VALUE, P.CTN_COUNT, ISNULL(T.DELIVERY1ST +' - '+ S.RSN_DESC,'') AS DVL_RSN, BALANCE, P.BLOCKED_STATUS,T.ROUND_NUM,T.DELIVERY1ST,T.DEVOL_RSN FROM PAYMENT P INNER JOIN REPRESENTATIVES R ON P.ACCOUNT = R.ACCOUNT LEFT OUTER JOIN PAYMENTT T ON P.ACCOUNT=T.ACCOUNT AND P.CAMP_YEAR=T.CAMP_YEAR AND P.CAMPAIGN=T.CAMPAIGN LEFT OUTER JOIN DEVOL_RSN S ON T.DELIVERY1ST = S.DEVOL_RSN WHERE P.CAMP_YEAR = ? AND  P.CAMPAIGN = ? AND P.ZONE = ? AND T.HJK_TYP IS NULL ORDER BY R.SECTION ASC, P.SLIP_VALUE DESC ", valores));
	    } 
	    return listOrdenesDev;
	  }
	  
	  public static List<OrdenZonaBean> obtenerOrdenesRed(int anio, int campania, int zona) {
	    List<OrdenZonaBean> listOrdenesRed = new ArrayList<>();
	    Query query = new Query();
	    ResultSet rs = null;
	    ArrayList<Object> valores = new ArrayList();
	    valores.add(Integer.valueOf(anio));
	    valores.add(Integer.valueOf(campania));
	    valores.add(Integer.valueOf(zona));
	    query.createQuery("SELECT R.SECTION, P.ACCOUNT, ISNULL(P.MODIF_VAL,P.COD_VALUE) COD_VALUE FROM PAYMENT P INNER JOIN REPRESENTATIVES R ON P.ACCOUNT = R.ACCOUNT WHERE P.CAMP_YEAR = ? AND  P.CAMPAIGN = ? AND P.ZONE = ?  ORDER BY R.SECTION ASC, P.COD_VALUE DESC ", valores);
	    if (query.executeSelect()) {
	      rs = query.getResultSet();
	      try {
	        while (rs.next()) {
	          OrdenZonaBean bean = new OrdenZonaBean();
	          bean.setRegistro(rs.getInt("ACCOUNT"));
	          bean.setRed(rs.getInt("SECTION"));
	          bean.setValorCod(rs.getFloat("COD_VALUE"));
	          listOrdenesRed.add(bean);
	        } 
	      } catch (SQLException e) {
	        e.printStackTrace();
	        //loggerError.error("Error en query" + e);
	      } finally {
	        query.closeQuey();
	        query = null;
	      } 
	    } else {
	      //loggerError.error("Error en query: " + query.getQuery("SELECT R.SECTION, P.ACCOUNT, ISNULL(P.MODIF_VAL,P.COD_VALUE) COD_VALUE FROM PAYMENT P INNER JOIN REPRESENTATIVES R ON P.ACCOUNT = R.ACCOUNT WHERE P.CAMP_YEAR = ? AND  P.CAMPAIGN = ? AND P.ZONE = ?  ORDER BY R.SECTION ASC, P.COD_VALUE DESC ", valores));
	    } 
	    return listOrdenesRed;
	  }
	  
	  public static int obtenerTotalOrdenesLibresNoEntregadas(int anio, int campania, int zona) {
	    Query query = new Query();
	    ResultSet rs = null;
	    int totOrdLibresNoEntr = 0;
	    ArrayList<Object> valores = new ArrayList();
	    valores.add(Integer.valueOf(anio));
	    valores.add(Integer.valueOf(campania));
	    valores.add(Integer.valueOf(zona));
	    query.createQuery("SELECT count(P.ACCOUNT) CTN_CARTON FROM PAYMENT P INNER JOIN REPRESENTATIVES R ON P.ACCOUNT = R.ACCOUNT LEFT OUTER JOIN PAYMENTT T ON P.ACCOUNT=T.ACCOUNT AND P.CAMP_YEAR=T.CAMP_YEAR AND P.CAMPAIGN=T.CAMPAIGN LEFT OUTER JOIN DEVOL_RSN S ON T.DELIVERY1ST = S.DEVOL_RSN WHERE P.CAMP_YEAR = ? AND  P.CAMPAIGN = ? AND P.ZONE = ? AND ((P.MODIF_VAL IS NULL AND P.COD_VALUE = 0)OR P.MODIF_VAL = 0) AND T.DEVOL_RSN IS NULL AND T.ROUND_NUM IS NULL AND T.HJK_TYP IS NULL ", valores);
	    if (query.executeSelect()) {
	      rs = query.getResultSet();
	      try {
	        if (rs.next())
	          totOrdLibresNoEntr = rs.getInt("CTN_CARTON"); 
	      } catch (SQLException e) {
	        e.printStackTrace();
	        //loggerError.error("Error en query" + e);
	      } finally {
	        query.closeQuey();
	        query = null;
	      } 
	    } else {
	      //loggerError.error("Error en query: " + query.getQuery("SELECT count(P.ACCOUNT) CTN_CARTON FROM PAYMENT P INNER JOIN REPRESENTATIVES R ON P.ACCOUNT = R.ACCOUNT LEFT OUTER JOIN PAYMENTT T ON P.ACCOUNT=T.ACCOUNT AND P.CAMP_YEAR=T.CAMP_YEAR AND P.CAMPAIGN=T.CAMPAIGN LEFT OUTER JOIN DEVOL_RSN S ON T.DELIVERY1ST = S.DEVOL_RSN WHERE P.CAMP_YEAR = ? AND  P.CAMPAIGN = ? AND P.ZONE = ? AND ((P.MODIF_VAL IS NULL AND P.COD_VALUE = 0)OR P.MODIF_VAL = 0) AND T.DEVOL_RSN IS NULL AND T.ROUND_NUM IS NULL AND T.HJK_TYP IS NULL ", valores));
	    } 
	    return totOrdLibresNoEntr;
	  }
	  
	  public static int obtenerTotalOrdenesLibres(int anio, int campania, int zona) {
	    Query query = new Query();
	    ResultSet rs = null;
	    int totOrdLibres = 0;
	    ArrayList<Object> valores = new ArrayList();
	    valores.add(Integer.valueOf(anio));
	    valores.add(Integer.valueOf(campania));
	    valores.add(Integer.valueOf(zona));
	    valores.add(Integer.valueOf(anio));
	    valores.add(Integer.valueOf(campania));
	    valores.add(Integer.valueOf(zona));
	    query.createQuery("SELECT count(P.ACCOUNT)TOT_CARTON FROM PAYMENT P INNER JOIN REPRESENTATIVES R ON P.ACCOUNT = R.ACCOUNT LEFT OUTER JOIN PAYMENTT T ON P.ACCOUNT=T.ACCOUNT AND P.CAMP_YEAR=T.CAMP_YEAR AND P.CAMPAIGN=T.CAMPAIGN LEFT OUTER JOIN DEVOL_RSN S ON T.DELIVERY1ST = S.DEVOL_RSN WHERE P.CAMP_YEAR = ? AND  P.CAMPAIGN = ? AND P.ZONE = ? AND ((P.MODIF_VAL IS NULL AND P.COD_VALUE = 0)OR P.MODIF_VAL = 0) AND R.SECTION IN (SELECT R.SECTION FROM PAYMENT P INNER JOIN REPRESENTATIVES R ON P.ACCOUNT = R.ACCOUNT LEFT OUTER JOIN PAYMENTT T ON P.ACCOUNT=T.ACCOUNT AND P.CAMP_YEAR=T.CAMP_YEAR AND P.CAMPAIGN=T.CAMPAIGN LEFT OUTER JOIN DEVOL_RSN S ON T.DELIVERY1ST = S.DEVOL_RSN WHERE P.CAMP_YEAR = ? AND  P.CAMPAIGN = ? AND P.ZONE = ? )", valores);
	    if (query.executeSelect()) {
	      rs = query.getResultSet();
	      try {
	        if (rs.next())
	          totOrdLibres = rs.getInt("TOT_CARTON"); 
	      } catch (SQLException e) {
	        e.printStackTrace();
	        //loggerError.error("Error en query" + e);
	      } finally {
	        query.closeQuey();
	        query = null;
	      } 
	    } else {
	      //loggerError.error("Error en query: " + query.getQuery("SELECT count(P.ACCOUNT)TOT_CARTON FROM PAYMENT P INNER JOIN REPRESENTATIVES R ON P.ACCOUNT = R.ACCOUNT LEFT OUTER JOIN PAYMENTT T ON P.ACCOUNT=T.ACCOUNT AND P.CAMP_YEAR=T.CAMP_YEAR AND P.CAMPAIGN=T.CAMPAIGN LEFT OUTER JOIN DEVOL_RSN S ON T.DELIVERY1ST = S.DEVOL_RSN WHERE P.CAMP_YEAR = ? AND  P.CAMPAIGN = ? AND P.ZONE = ? AND ((P.MODIF_VAL IS NULL AND P.COD_VALUE = 0)OR P.MODIF_VAL = 0) AND R.SECTION IN (SELECT R.SECTION FROM PAYMENT P INNER JOIN REPRESENTATIVES R ON P.ACCOUNT = R.ACCOUNT LEFT OUTER JOIN PAYMENTT T ON P.ACCOUNT=T.ACCOUNT AND P.CAMP_YEAR=T.CAMP_YEAR AND P.CAMPAIGN=T.CAMPAIGN LEFT OUTER JOIN DEVOL_RSN S ON T.DELIVERY1ST = S.DEVOL_RSN WHERE P.CAMP_YEAR = ? AND  P.CAMPAIGN = ? AND P.ZONE = ? )", valores));
	    } 
	    return totOrdLibres;
	  }
	  
	  public static float obtenerSumaTotalOrdenesLibres(int anio, int campania, int zona) {
	    Query query = new Query();
	    ResultSet rs = null;
	    float total = 0.0F;
	    ArrayList<Object> valores = new ArrayList();
	    valores.add(Integer.valueOf(anio));
	    valores.add(Integer.valueOf(campania));
	    valores.add(Integer.valueOf(zona));
	    query.createQuery("SELECT sum(P.SLIP_VALUE) SUM_SLIP_VALUE FROM PAYMENT P INNER JOIN REPRESENTATIVES R ON P.ACCOUNT = R.ACCOUNT LEFT OUTER JOIN PAYMENTT T ON P.ACCOUNT=T.ACCOUNT AND P.CAMP_YEAR=T.CAMP_YEAR AND P.CAMPAIGN=T.CAMPAIGN LEFT OUTER JOIN DEVOL_RSN S ON T.DELIVERY1ST = S.DEVOL_RSN WHERE P.CAMP_YEAR = ? AND  P.CAMPAIGN = ? AND P.ZONE = ? AND ((P.MODIF_VAL IS NULL AND P.COD_VALUE = 0)OR P.MODIF_VAL = 0) AND T.DEVOL_RSN IS NULL AND T.ROUND_NUM IS NULL AND T.HJK_TYP IS NULL ", valores);
	    if (query.executeSelect()) {
	      rs = query.getResultSet();
	      try {
	        if (rs.next())
	          total = rs.getFloat("SUM_SLIP_VALUE"); 
	      } catch (SQLException e) {
	        e.printStackTrace();
	        //loggerError.error("Error en query" + e);
	        return 0.0F;
	      } finally {
	        query.closeQuey();
	        query = null;
	      } 
	    } else {
	      //loggerError.error("Error en query: " + query.getQuery("SELECT sum(P.SLIP_VALUE) SUM_SLIP_VALUE FROM PAYMENT P INNER JOIN REPRESENTATIVES R ON P.ACCOUNT = R.ACCOUNT LEFT OUTER JOIN PAYMENTT T ON P.ACCOUNT=T.ACCOUNT AND P.CAMP_YEAR=T.CAMP_YEAR AND P.CAMPAIGN=T.CAMPAIGN LEFT OUTER JOIN DEVOL_RSN S ON T.DELIVERY1ST = S.DEVOL_RSN WHERE P.CAMP_YEAR = ? AND  P.CAMPAIGN = ? AND P.ZONE = ? AND ((P.MODIF_VAL IS NULL AND P.COD_VALUE = 0)OR P.MODIF_VAL = 0) AND T.DEVOL_RSN IS NULL AND T.ROUND_NUM IS NULL AND T.HJK_TYP IS NULL ", valores));
	    } 
	    return total;
	  }
	  
	  public static List<OrdenRedBean> obtenerTotalesRed(int anio, int campania, int zona) {
	    List<OrdenRedBean> listTotalesRed = new ArrayList<>();
	    Query query = new Query();
	    ResultSet rs = null;
	    ArrayList<Object> valores = new ArrayList();
	    valores.add(Integer.valueOf(anio));
	    valores.add(Integer.valueOf(campania));
	    valores.add(Integer.valueOf(zona));
	    listTotalesRed.add(null);
	    for (int i = 1; i < 100; i++) {
	      OrdenRedBean bean = new OrdenRedBean();
	      bean.setRed(i);
	      bean.setAceptacionRed("%");
	      bean.setOrdenesDevolucion(0);
	      bean.setSumatoriaRed(0.0F);
	      bean.setTotalOrdenes(0);
	      bean.setOrdenesLibres(0);
	      bean.setSumatoriaLibres(0.0F);
	      bean.setTotalLibres(0);
	      bean.setAceptacionLibres("%");
	      listTotalesRed.add(bean);
	    } 
	    query.createQuery("SELECT R.SECTION, count(P.ACCOUNT) CTN_CARTON, sum(P.SLIP_VALUE) SUM_SLIP_VALUE FROM PAYMENT P INNER JOIN REPRESENTATIVES R ON P.ACCOUNT = R.ACCOUNT LEFT OUTER JOIN PAYMENTT T ON P.ACCOUNT=T.ACCOUNT AND P.CAMP_YEAR=T.CAMP_YEAR AND P.CAMPAIGN=T.CAMPAIGN LEFT OUTER JOIN DEVOL_RSN S ON T.DELIVERY1ST = S.DEVOL_RSN WHERE P.CAMP_YEAR = ? AND  P.CAMPAIGN = ? AND P.ZONE = ? AND T.DEVOL_RSN IS NULL AND T.ROUND_NUM IS NULL AND T.HJK_TYP IS NULL GROUP BY R.SECTION ORDER BY R.SECTION ASC", valores);
	    if (query.executeSelect()) {
	      rs = query.getResultSet();
	      try {
	        while (rs.next()) {
	          OrdenRedBean bean = listTotalesRed.get(rs.getInt("SECTION"));
	          bean.setOrdenesDevolucion(rs.getInt("CTN_CARTON"));
	          bean.setSumatoriaRed(rs.getFloat("SUM_SLIP_VALUE"));
	        } 
	      } catch (SQLException e) {
	        e.printStackTrace();
	        //loggerError.error("Error en query" + e);
	      } finally {
	        query = null;
	        rs = null;
	      } 
	    } else {
	      //loggerError.error("Error en query: " + query.getQuery("SELECT R.SECTION, count(P.ACCOUNT) CTN_CARTON, sum(P.SLIP_VALUE) SUM_SLIP_VALUE FROM PAYMENT P INNER JOIN REPRESENTATIVES R ON P.ACCOUNT = R.ACCOUNT LEFT OUTER JOIN PAYMENTT T ON P.ACCOUNT=T.ACCOUNT AND P.CAMP_YEAR=T.CAMP_YEAR AND P.CAMPAIGN=T.CAMPAIGN LEFT OUTER JOIN DEVOL_RSN S ON T.DELIVERY1ST = S.DEVOL_RSN WHERE P.CAMP_YEAR = ? AND  P.CAMPAIGN = ? AND P.ZONE = ? AND T.DEVOL_RSN IS NULL AND T.ROUND_NUM IS NULL AND T.HJK_TYP IS NULL GROUP BY R.SECTION ORDER BY R.SECTION ASC", valores));
	    } 
	    query = new Query();
	    ArrayList<Object> valores2 = new ArrayList();
	    valores2.add(Integer.valueOf(anio));
	    valores2.add(Integer.valueOf(campania));
	    valores2.add(Integer.valueOf(zona));
	    valores2.add(Integer.valueOf(anio));
	    valores2.add(Integer.valueOf(campania));
	    valores2.add(Integer.valueOf(zona));
	    query.createQuery("SELECT R.SECTION, count(P.ACCOUNT)TOT_CARTON FROM PAYMENT P INNER JOIN REPRESENTATIVES R ON P.ACCOUNT = R.ACCOUNT LEFT OUTER JOIN PAYMENTT T ON P.ACCOUNT=T.ACCOUNT AND P.CAMP_YEAR=T.CAMP_YEAR AND P.CAMPAIGN=T.CAMPAIGN LEFT OUTER JOIN DEVOL_RSN S ON T.DELIVERY1ST = S.DEVOL_RSN WHERE  P.CAMP_YEAR = ? AND  P.CAMPAIGN = ? AND P.ZONE = ? AND R.SECTION IN (SELECT R.SECTION FROM PAYMENT P INNER JOIN REPRESENTATIVES R ON P.ACCOUNT = R.ACCOUNT LEFT OUTER JOIN PAYMENTT T ON P.ACCOUNT=T.ACCOUNT AND P.CAMP_YEAR=T.CAMP_YEAR AND P.CAMPAIGN=T.CAMPAIGN LEFT OUTER JOIN DEVOL_RSN S ON T.DELIVERY1ST = S.DEVOL_RSN WHERE P.CAMP_YEAR = ? AND  P.CAMPAIGN = ? AND P.ZONE = ? AND T.DEVOL_RSN IS NULL AND T.ROUND_NUM IS NULL AND T.HJK_TYP IS NULL) GROUP BY R.SECTION ORDER BY R.SECTION ASC", valores2);
	    if (query.executeSelect()) {
	      rs = query.getResultSet();
	      try {
	        while (rs.next()) {
	          OrdenRedBean bean = listTotalesRed.get(rs.getInt("SECTION"));
	          bean.setTotalOrdenes(rs.getInt("TOT_CARTON"));
	        } 
	      } catch (SQLException e) {
	        e.printStackTrace();
	        //loggerError.error("Error en query" + e);
	      } finally {
	        query = null;
	        rs = null;
	      } 
	    } else {
	      //loggerError.error("Error en query: " + query.getQuery("SELECT R.SECTION, count(P.ACCOUNT)TOT_CARTON FROM PAYMENT P INNER JOIN REPRESENTATIVES R ON P.ACCOUNT = R.ACCOUNT LEFT OUTER JOIN PAYMENTT T ON P.ACCOUNT=T.ACCOUNT AND P.CAMP_YEAR=T.CAMP_YEAR AND P.CAMPAIGN=T.CAMPAIGN LEFT OUTER JOIN DEVOL_RSN S ON T.DELIVERY1ST = S.DEVOL_RSN WHERE  P.CAMP_YEAR = ? AND  P.CAMPAIGN = ? AND P.ZONE = ? AND R.SECTION IN (SELECT R.SECTION FROM PAYMENT P INNER JOIN REPRESENTATIVES R ON P.ACCOUNT = R.ACCOUNT LEFT OUTER JOIN PAYMENTT T ON P.ACCOUNT=T.ACCOUNT AND P.CAMP_YEAR=T.CAMP_YEAR AND P.CAMPAIGN=T.CAMPAIGN LEFT OUTER JOIN DEVOL_RSN S ON T.DELIVERY1ST = S.DEVOL_RSN WHERE P.CAMP_YEAR = ? AND  P.CAMPAIGN = ? AND P.ZONE = ? AND T.DEVOL_RSN IS NULL AND T.ROUND_NUM IS NULL AND T.HJK_TYP IS NULL) GROUP BY R.SECTION ORDER BY R.SECTION ASC", valores));
	    } 
	    query = new Query();
	    query.createQuery("SELECT R.SECTION, count(P.ACCOUNT) CTN_CARTON, sum(P.SLIP_VALUE) SUM_SLIP_VALUE FROM PAYMENT P INNER JOIN REPRESENTATIVES R ON P.ACCOUNT = R.ACCOUNT LEFT OUTER JOIN PAYMENTT T ON P.ACCOUNT=T.ACCOUNT AND P.CAMP_YEAR=T.CAMP_YEAR AND P.CAMPAIGN=T.CAMPAIGN LEFT OUTER JOIN DEVOL_RSN S ON T.DELIVERY1ST = S.DEVOL_RSN WHERE P.CAMP_YEAR = ? AND  P.CAMPAIGN = ? AND P.ZONE = ? AND ((P.MODIF_VAL IS NULL AND P.COD_VALUE = 0)OR P.MODIF_VAL = 0) AND T.DEVOL_RSN IS NULL AND T.ROUND_NUM IS NULL AND T.HJK_TYP IS NULL GROUP BY R.SECTION ORDER BY R.SECTION ASC ", valores);
	    if (query.executeSelect()) {
	      rs = query.getResultSet();
	      try {
	        while (rs.next()) {
	          OrdenRedBean bean = listTotalesRed.get(rs.getInt("SECTION"));
	          bean.setOrdenesLibres(rs.getInt("CTN_CARTON"));
	          bean.setSumatoriaLibres(rs.getFloat("SUM_SLIP_VALUE"));
	        } 
	      } catch (SQLException e) {
	        e.printStackTrace();
	        //loggerError.error("Error en query" + e);
	      } finally {
	        query = null;
	        rs = null;
	      } 
	    } else {
	      //loggerError.error("Error en query: " + query.getQuery("SELECT R.SECTION, count(P.ACCOUNT) CTN_CARTON, sum(P.SLIP_VALUE) SUM_SLIP_VALUE FROM PAYMENT P INNER JOIN REPRESENTATIVES R ON P.ACCOUNT = R.ACCOUNT LEFT OUTER JOIN PAYMENTT T ON P.ACCOUNT=T.ACCOUNT AND P.CAMP_YEAR=T.CAMP_YEAR AND P.CAMPAIGN=T.CAMPAIGN LEFT OUTER JOIN DEVOL_RSN S ON T.DELIVERY1ST = S.DEVOL_RSN WHERE P.CAMP_YEAR = ? AND  P.CAMPAIGN = ? AND P.ZONE = ? AND ((P.MODIF_VAL IS NULL AND P.COD_VALUE = 0)OR P.MODIF_VAL = 0) AND T.DEVOL_RSN IS NULL AND T.ROUND_NUM IS NULL AND T.HJK_TYP IS NULL GROUP BY R.SECTION ORDER BY R.SECTION ASC ", valores));
	    } 
	    query = new Query();
	    query.createQuery("SELECT R.SECTION, count(P.ACCOUNT)TOT_CARTON FROM PAYMENT P INNER JOIN REPRESENTATIVES R ON P.ACCOUNT = R.ACCOUNT LEFT OUTER JOIN PAYMENTT T ON P.ACCOUNT=T.ACCOUNT AND P.CAMP_YEAR=T.CAMP_YEAR AND P.CAMPAIGN=T.CAMPAIGN LEFT OUTER JOIN DEVOL_RSN S ON T.DELIVERY1ST = S.DEVOL_RSN WHERE  P.CAMP_YEAR = ? AND  P.CAMPAIGN = ? AND P.ZONE = ? AND ((P.MODIF_VAL IS NULL AND P.COD_VALUE = 0)OR P.MODIF_VAL = 0) AND R.SECTION IN (SELECT R.SECTION FROM PAYMENT P INNER JOIN REPRESENTATIVES R ON P.ACCOUNT = R.ACCOUNT LEFT OUTER JOIN PAYMENTT T ON P.ACCOUNT=T.ACCOUNT AND P.CAMP_YEAR=T.CAMP_YEAR AND P.CAMPAIGN=T.CAMPAIGN LEFT OUTER JOIN DEVOL_RSN S ON T.DELIVERY1ST = S.DEVOL_RSN WHERE  P.CAMP_YEAR = ? AND  P.CAMPAIGN = ? AND P.ZONE = ? ) GROUP BY R.SECTION ORDER BY R.SECTION ASC ", valores2);
	    if (query.executeSelect()) {
	      rs = query.getResultSet();
	      try {
	        while (rs.next()) {
	          OrdenRedBean bean = listTotalesRed.get(rs.getInt("SECTION"));
	          bean.setTotalLibres(rs.getInt("TOT_CARTON"));
	        } 
	      } catch (SQLException e) {
	        e.printStackTrace();
	        //loggerError.error("Error en query" + e);
	      } finally {
	        query = null;
	        rs = null;
	      } 
	    } else {
	      //loggerError.error("Error en query: " + query.getQuery("SELECT R.SECTION, count(P.ACCOUNT)TOT_CARTON FROM PAYMENT P INNER JOIN REPRESENTATIVES R ON P.ACCOUNT = R.ACCOUNT LEFT OUTER JOIN PAYMENTT T ON P.ACCOUNT=T.ACCOUNT AND P.CAMP_YEAR=T.CAMP_YEAR AND P.CAMPAIGN=T.CAMPAIGN LEFT OUTER JOIN DEVOL_RSN S ON T.DELIVERY1ST = S.DEVOL_RSN WHERE  P.CAMP_YEAR = ? AND  P.CAMPAIGN = ? AND P.ZONE = ? AND ((P.MODIF_VAL IS NULL AND P.COD_VALUE = 0)OR P.MODIF_VAL = 0) AND R.SECTION IN (SELECT R.SECTION FROM PAYMENT P INNER JOIN REPRESENTATIVES R ON P.ACCOUNT = R.ACCOUNT LEFT OUTER JOIN PAYMENTT T ON P.ACCOUNT=T.ACCOUNT AND P.CAMP_YEAR=T.CAMP_YEAR AND P.CAMPAIGN=T.CAMPAIGN LEFT OUTER JOIN DEVOL_RSN S ON T.DELIVERY1ST = S.DEVOL_RSN WHERE  P.CAMP_YEAR = ? AND  P.CAMPAIGN = ? AND P.ZONE = ? ) GROUP BY R.SECTION ORDER BY R.SECTION ASC ", valores));
	    } 
	    return listTotalesRed;
	  }
	  
	  public static boolean revisaExisteReparto(int anio, int camp, int zona) {
	    int numPayment = 0;
	    int numPaymentt = 0;
	    boolean existReparto = true;
	    numPayment = obtenerCantidadOrdenesDevolucion(anio, camp, zona);
	    numPaymentt = obtenerCantidadTotalOrdenes(anio, camp, zona);
	    if (numPaymentt == numPayment)
	      existReparto = false; 
	    return existReparto;
	  }
	  
	  public static int obtenerCantidadOrdenesDevueltas(int anio, int campania, int zona) {
	    Query query = new Query();
	    ResultSet rs = null;
	    int totOrdDev = 0;
	    ArrayList<Object> valores = new ArrayList();
	    valores.add(Integer.valueOf(anio));
	    valores.add(Integer.valueOf(campania));
	    valores.add(Integer.valueOf(zona));
	    query.createQuery("SELECT COUNT(P.ACCOUNT) AS TOT_ORD_DEV FROM PAYMENT P LEFT OUTER JOIN PAYMENTT T ON P.ACCOUNT=T.ACCOUNT AND P.CAMP_YEAR=T.CAMP_YEAR AND P.CAMPAIGN=T.CAMPAIGN WHERE T.DEVOL_RSN IS NOT NULL AND P.CAMP_YEAR = ? AND P.CAMPAIGN = ? AND P.ZONE= ? ", valores);
	    if (query.executeSelect()) {
	      rs = query.getResultSet();
	      try {
	        if (rs.next())
	          totOrdDev = rs.getInt("TOT_ORD_DEV"); 
	      } catch (SQLException e) {
	        e.printStackTrace();
	        //loggerError.error("Error en query" + e);
	      } finally {
	        query.closeQuey();
	        query = null;
	      } 
	    } else {
	      //loggerError.error("Error en query: " + query.getQuery("SELECT COUNT(P.ACCOUNT) AS TOT_ORD_DEV FROM PAYMENT P LEFT OUTER JOIN PAYMENTT T ON P.ACCOUNT=T.ACCOUNT AND P.CAMP_YEAR=T.CAMP_YEAR AND P.CAMPAIGN=T.CAMPAIGN WHERE T.DEVOL_RSN IS NOT NULL AND P.CAMP_YEAR = ? AND P.CAMPAIGN = ? AND P.ZONE= ? ", valores));
	    } 
	    return totOrdDev;
	  }
	  
	  public static float obtenerSumaTotalOrdenesDevueltas(int anio, int campania, int zona) {
	    Query query = new Query();
	    ResultSet rs = null;
	    int sumOrdDev = 0;
	    ArrayList<Object> valores = new ArrayList();
	    valores.add(Integer.valueOf(anio));
	    valores.add(Integer.valueOf(campania));
	    valores.add(Integer.valueOf(zona));
	    query.createQuery("SELECT SUM(P.SLIP_VALUE) AS SUM_ORD_DEV FROM PAYMENT P LEFT OUTER JOIN PAYMENTT T ON P.ACCOUNT=T.ACCOUNT AND P.CAMP_YEAR=T.CAMP_YEAR AND P.CAMPAIGN=T.CAMPAIGN WHERE T.DEVOL_RSN IS NOT NULL AND P.CAMP_YEAR = ? AND P.CAMPAIGN = ? AND P.ZONE = ?", valores);
	    if (query.executeSelect()) {
	      rs = query.getResultSet();
	      try {
	        if (rs.next())
	          sumOrdDev = rs.getInt("SUM_ORD_DEV"); 
	      } catch (SQLException e) {
	        e.printStackTrace();
	        //loggerError.error("Error en query" + e);
	      } finally {
	        query.closeQuey();
	        query = null;
	      } 
	    } else {
	      //loggerError.error("Error en query: " + query.getQuery("SELECT SUM(P.SLIP_VALUE) AS SUM_ORD_DEV FROM PAYMENT P LEFT OUTER JOIN PAYMENTT T ON P.ACCOUNT=T.ACCOUNT AND P.CAMP_YEAR=T.CAMP_YEAR AND P.CAMPAIGN=T.CAMPAIGN WHERE T.DEVOL_RSN IS NOT NULL AND P.CAMP_YEAR = ? AND P.CAMPAIGN = ? AND P.ZONE = ?", valores));
	    } 
	    return sumOrdDev;
	  }
	  
	  public static List<OrdenBean> obtenerOrdenesDevueltas(int anio, int campania, int zona) {
	    List<OrdenBean> listOrdenesDev = new ArrayList<>();
	    Query query = new Query();
	    ResultSet rs = null;
	    ArrayList<Object> valores = new ArrayList();
	    valores.add(Integer.valueOf(anio));
	    valores.add(Integer.valueOf(campania));
	    valores.add(Integer.valueOf(zona));
	    query.createQuery("SELECT R.SECTION, P.ACCOUNT, R.REP_NAME,R.ADDRESS1, R.ADDRESS2, R.ADDRESS3, ISNULL(P.MODIF_VAL,P.COD_VALUE) COD_VALUE, P.SLIP_VALUE, P.CTN_COUNT, T.DELIVERY1ST +' - '+ S.RSN_DESC AS DVL_RSN, BALANCE FROM PAYMENT P INNER JOIN REPRESENTATIVES R ON P.ACCOUNT = R.ACCOUNT LEFT OUTER JOIN PAYMENTT T ON P.ACCOUNT=T.ACCOUNT AND P.CAMP_YEAR=T.CAMP_YEAR AND P.CAMPAIGN=T.CAMPAIGN LEFT OUTER JOIN DEVOL_RSN S ON T.DELIVERY1ST = S.DEVOL_RSN WHERE T.DEVOL_RSN IS NOT NULL AND P.CAMP_YEAR = ? AND  P.CAMPAIGN = ? AND P.ZONE = ? ORDER BY R.SECTION ASC, P.SLIP_VALUE DESC", valores);
	    if (query.executeSelect()) {
	      rs = query.getResultSet();
	      try {
	        while (rs.next()) {
	          OrdenBean bean = new OrdenBean();
	          bean.setRegistro(Integer.valueOf(rs.getInt("ACCOUNT")));
	          bean.setNombre(rs.getString("REP_NAME"));
	          bean.setDireccion(rs.getString("ADDRESS1"));
	          bean.setCalles(rs.getString("ADDRESS2"));
	          bean.setPoblacion(rs.getString("ADDRESS3"));
	          bean.setValorCod(rs.getFloat("COD_VALUE"));
	          bean.setValorPedido(rs.getFloat("SLIP_VALUE"));
	          bean.setRed(rs.getInt("SECTION"));
	          bean.setCajas(Integer.valueOf(rs.getInt("CTN_COUNT")));
	          bean.setCausa(rs.getString("DVL_RSN"));
	          bean.setBalance(rs.getFloat("BALANCE"));
	          listOrdenesDev.add(bean);
	        } 
	      } catch (SQLException e) {
	        e.printStackTrace();
	        //loggerError.error("Error en query" + e);
	      } finally {
	        query.closeQuey();
	        query = null;
	      } 
	    } else {
	      //loggerError.error("Error en query: " + query.getQuery("SELECT R.SECTION, P.ACCOUNT, R.REP_NAME,R.ADDRESS1, R.ADDRESS2, R.ADDRESS3, ISNULL(P.MODIF_VAL,P.COD_VALUE) COD_VALUE, P.SLIP_VALUE, P.CTN_COUNT, T.DELIVERY1ST +' - '+ S.RSN_DESC AS DVL_RSN, BALANCE FROM PAYMENT P INNER JOIN REPRESENTATIVES R ON P.ACCOUNT = R.ACCOUNT LEFT OUTER JOIN PAYMENTT T ON P.ACCOUNT=T.ACCOUNT AND P.CAMP_YEAR=T.CAMP_YEAR AND P.CAMPAIGN=T.CAMPAIGN LEFT OUTER JOIN DEVOL_RSN S ON T.DELIVERY1ST = S.DEVOL_RSN WHERE T.DEVOL_RSN IS NOT NULL AND P.CAMP_YEAR = ? AND  P.CAMPAIGN = ? AND P.ZONE = ? ORDER BY R.SECTION ASC, P.SLIP_VALUE DESC", valores));
	    } 
	    return listOrdenesDev;
	  }
}
