package com.avon.repNarFronterasAbiertas.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;


public class Parametros {
	
	//private String directorioArchivoPropiedades = "/Apps/scheduler/Apps/OTSTools/fronterasAbiertasV2/config.properties";
	private String directorioArchivoPropiedades = "config.properties";
	
    private Properties propiedades;
    
    private void cargarPropiedades() {
    	InputStream isrParams = null;
    	propiedades = new Properties();
    	try {
    		isrParams = Parametros.class.getClassLoader().getResourceAsStream(directorioArchivoPropiedades);
			propiedades.load(isrParams);
		} catch (IOException e) {
			e.printStackTrace();
		}
    }
    
    public String recuperarPropiedad(String key) {
    	String valorPropiedad = "";
    	
    	cargarPropiedades();
    	valorPropiedad = propiedades.getProperty(key);
    	return valorPropiedad;
    }
    
    /*
    private void cargarPropiedades() {
    	InputStream isrParams = null;
    	propiedades = new Properties();
    	try {
    		isrParams = new FileInputStream(directorioArchivoPropiedades);
			propiedades.load(isrParams);
		} catch (IOException e) {
			e.printStackTrace();
		}
    }
    
    public String recuperarPropiedad(String key) {
    	String valorPropiedad = "";
    	
    	cargarPropiedades();
    	valorPropiedad = propiedades.getProperty(key);
    	return valorPropiedad;
    }
    */
}
