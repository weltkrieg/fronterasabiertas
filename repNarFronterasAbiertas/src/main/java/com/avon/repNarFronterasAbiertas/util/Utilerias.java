package com.avon.repNarFronterasAbiertas.util;

import java.io.File;
import java.util.ArrayList;

public class Utilerias {

	public static void verificaDirectorio(String rutaFile) {
	    File folder = new File(rutaFile);
	    try {
	      if (!folder.exists()) {
	        creaDirectorio(rutaFile);
	        //loggerInfo.info("Se ha creado el Directorio: " + rutaFile);
	      } 
	    } catch (Exception e) {
	      //loggerError.error("Exception", e);
	    } 
	  }
	  
	  public static void creaDirectorio(String rutaFile) {
	    try {
	      File folder = new File(rutaFile);
	      folder.mkdir();
	    } catch (Exception e) {
	      //loggerError.error("Error al crear el directorio:", e);
	    } 
	  }
	  
	  public static String completar(int max, int num) {
	    String temp = "" + num;
	    if (max > temp.length()) {
	      int cont = max - temp.length();
	      for (int i = 0; i < cont; i++)
	        temp = "0" + temp; 
	    } 
	    return temp;
	  }
	  
	  public static ArrayList<Integer> obtenerCampaniaAnterior(int anio, int camp) {
	    ArrayList<Integer> campAnt = new ArrayList<>();
	    if (camp == 1) {
	      campAnt.add(Integer.valueOf(anio - 1));
	      campAnt.add(Integer.valueOf(20));
	    } else {
	      campAnt.add(Integer.valueOf(anio));
	      campAnt.add(Integer.valueOf(camp - 1));
	    } 
	    return campAnt;
	  }
}
