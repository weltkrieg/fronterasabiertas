package com.avon.repNarFronterasAbiertas.reporte;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.VerticalAlignment;

public class ArchivoExcel2003 {

	private HSSFWorkbook workbook;
	  
	  private int noSheets = 0;
	  
	  public void leerArchivo(String pathFile) {
	    try {
	      this.workbook = null;
	      FileInputStream file = new FileInputStream(new File(pathFile));
	      this.workbook = new HSSFWorkbook(file);
	      this.noSheets = this.workbook.getNumberOfSheets();
	    } catch (FileNotFoundException e) {
	      //loggerError.info("Archivo no encontrado");
	      //loggerError.error("Archivo no encontrado!: " + e);
	      e.printStackTrace();
	    } catch (IOException e) {
	      //loggerError.info("Hubo un error al leer el archivo");
	      //loggerError.error("Error al leer el archivo!: " + e);
	      e.printStackTrace();
	    } 
	  }
	  
	  public void crearArchivoNuevo() {
	    if (this.workbook != null)
	      this.workbook = null; 
	    this.workbook = new HSSFWorkbook();
	  }
	  
	  public HSSFSheet agregarHoja(String nombreHoja) {
	    HSSFSheet hoja = this.workbook.createSheet(nombreHoja);
	    this.noSheets++;
	    return hoja;
	  }
	  
	  public HSSFSheet obtenerHoja(String nombreHoja) {
	    HSSFSheet hoja = this.workbook.getSheet(nombreHoja);
	    return hoja;
	  }
	  
	  public HSSFSheet obtenerHoja(int numHoja) {
	    HSSFSheet hoja = this.workbook.getSheetAt(numHoja);
	    return hoja;
	  }
	  
	  public List<Object> getDatos(int noHoja, int noFila) {
	    List<Object> datos = new ArrayList();
	    HSSFSheet sheet = obtenerHoja(noFila);
	    HSSFRow row = sheet.getRow(noFila);
	    Iterator<Cell> cellIter = row.cellIterator();
	    while (cellIter.hasNext()) {
	      HSSFCell cell = (HSSFCell)cellIter.next();
	      switch (cell.getCellType()) {
	        case NUMERIC:
	          if (HSSFDateUtil.isCellDateFormatted((Cell)cell)) {
	            datos.add(cell.getDateCellValue());
	            continue;
	          } 
	          datos.add(Double.valueOf(cell.getNumericCellValue()));
	        case STRING:
	          datos.add(cell.getStringCellValue());
	      } 
	    } 
	    return datos;
	  }
	  
	  public void setDatos(List<Object> listData, int noHoja, int noFila) {
	    HSSFSheet sheet = obtenerHoja(noFila);
	    HSSFRow row = sheet.getRow(noFila);
	    Iterator<Object> datIter = listData.iterator();
	    int numcell = 0;
	    while (datIter.hasNext()) {
	      HSSFCell cell = row.createCell(numcell);
	      Object dato = datIter.next();
	      if (dato instanceof Integer) {
	        cell.setCellValue(Integer.valueOf((String)dato).intValue());
	      } else if (dato instanceof Float) {
	        cell.setCellValue(Float.valueOf((String)dato).floatValue());
	      } else if (dato instanceof String) {
	        cell.setCellValue((String)dato);
	      } else if (dato instanceof Double) {
	        cell.setCellValue(Double.valueOf((String)dato).doubleValue());
	      } else if (dato instanceof Date) {
	        cell.setCellValue((Date)dato);
	      } else if (dato instanceof Long) {
	        cell.setCellValue(Long.valueOf((String)dato).longValue());
	      } else if (dato instanceof Timestamp) {
	        cell.setCellValue(Timestamp.valueOf((String)dato));
	      } else if (dato != null) {
	        cell.setCellValue((String)dato);
	      } 
	      numcell++;
	    } 
	  }
	  
	  public void guardarArchivo(String pathFile) {
	    OutputStream salida = null;
	    try {
	      salida = new FileOutputStream(pathFile);
	      if (this.workbook != null) {
	        this.workbook.write(salida);
	        salida.close();
	      } else {
	        //loggerError.error("No se puede guardar no hay ningun archivo de excel abierto.");
	      } 
	    } catch (FileNotFoundException e) {
	      //loggerError.info("Hubo un error con la ruta a donde guardar");
	      //loggerError.error("Hubo un error con la ruta a donde guardar!: " + e);
	      e.printStackTrace();
	    } catch (IOException e) {
	      //loggerError.info("Hubo un error al guardar el archivo");
	      //loggerError.error("Error al guardar el archivo!: " + e);
	      e.printStackTrace();
	    } 
	  }
	  
	  public CellStyle estiloBordes() {
	    if (this.workbook != null) {
	      HSSFCellStyle hSSFCellStyle = this.workbook.createCellStyle();
	      //hSSFCellStyle.setBorderBottom((short)1);
	      hSSFCellStyle.setBorderBottom(BorderStyle.HAIR);
	      hSSFCellStyle.setBottomBorderColor((short)8);
	      hSSFCellStyle.setBorderLeft(BorderStyle.HAIR);
	      hSSFCellStyle.setLeftBorderColor((short)8);
	      hSSFCellStyle.setBorderRight(BorderStyle.HAIR);
	      hSSFCellStyle.setRightBorderColor((short)8);
	      hSSFCellStyle.setBorderTop(BorderStyle.HAIR);
	      hSSFCellStyle.setTopBorderColor((short)8);
	      return (CellStyle)hSSFCellStyle;
	    } 
	    return null;
	  }
	  
	  public CellStyle estiloBordesDerecha() {
	    if (this.workbook != null) {
	      HSSFCellStyle hSSFCellStyle = this.workbook.createCellStyle();
	      hSSFCellStyle.setBorderBottom(BorderStyle.HAIR);
	      hSSFCellStyle.setBottomBorderColor((short)8);
	      hSSFCellStyle.setBorderLeft(BorderStyle.HAIR);
	      hSSFCellStyle.setLeftBorderColor((short)8);
	      hSSFCellStyle.setBorderRight(BorderStyle.HAIR);
	      hSSFCellStyle.setRightBorderColor((short)8);
	      hSSFCellStyle.setBorderTop(BorderStyle.HAIR);
	      hSSFCellStyle.setTopBorderColor((short)8);
	      hSSFCellStyle.setAlignment(HorizontalAlignment.LEFT);
	      return (CellStyle)hSSFCellStyle;
	    } 
	    return null;
	  }
	  
	  public CellStyle estiloTituloBordes() {
	    if (this.workbook != null) {
	      HSSFCellStyle hSSFCellStyle = this.workbook.createCellStyle();
	      hSSFCellStyle.setBorderBottom(BorderStyle.HAIR);
	      hSSFCellStyle.setBottomBorderColor((short)8);
	      hSSFCellStyle.setBorderLeft(BorderStyle.HAIR);
	      hSSFCellStyle.setLeftBorderColor((short)8);
	      hSSFCellStyle.setBorderRight(BorderStyle.HAIR);
	      hSSFCellStyle.setRightBorderColor((short)8);
	      hSSFCellStyle.setBorderTop(BorderStyle.HAIR);
	      hSSFCellStyle.setTopBorderColor((short)8);
	      hSSFCellStyle.setFont((Font)negrita());
	      return (CellStyle)hSSFCellStyle;
	    } 
	    return null;
	  }
	  
	  public CellStyle estiloTitulos() {
	    if (this.workbook != null) {
	      HSSFCellStyle hSSFCellStyle = this.workbook.createCellStyle();
	      hSSFCellStyle.setAlignment(HorizontalAlignment.CENTER);
	      hSSFCellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
	      hSSFCellStyle.setFont((Font)negrita());
	      return (CellStyle)hSSFCellStyle;
	    } 
	    return null;
	  }
	  
	  public CellStyle estiloDerecha() {
	    if (this.workbook != null) {
	      HSSFCellStyle hSSFCellStyle = this.workbook.createCellStyle();
	      hSSFCellStyle.setAlignment(HorizontalAlignment.RIGHT);
	      return (CellStyle)hSSFCellStyle;
	    } 
	    return null;
	  }
	  
	  public CellStyle estiloIzquierda() {
	    if (this.workbook != null) {
	      HSSFCellStyle hSSFCellStyle = this.workbook.createCellStyle();
	      hSSFCellStyle.setAlignment(HorizontalAlignment.LEFT);
	      return (CellStyle)hSSFCellStyle;
	    } 
	    return null;
	  }
	  
	  public CellStyle estiloCentro() {
	    if (this.workbook != null) {
	      HSSFCellStyle hSSFCellStyle = this.workbook.createCellStyle();
	      hSSFCellStyle.setAlignment(HorizontalAlignment.CENTER);
	      return (CellStyle)hSSFCellStyle;
	    } 
	    return null;
	  }
	  
	  public HSSFFont negrita() {
	    if (this.workbook != null) {
	      HSSFFont cellFont = this.workbook.createFont();
	      cellFont.setBold(true);
	      //cellFont.setBoldweight((short)700);
	      return cellFont;
	    } 
	    return null;
	  }
	  
	  public HSSFFont cursiva() {
	    if (this.workbook != null) {
	      HSSFFont cellFont = this.workbook.createFont();
	      cellFont.setItalic(true);
	      return cellFont;
	    } 
	    return null;
	  }
	  
	  public HSSFFont subrayado() {
	    if (this.workbook != null) {
	      HSSFFont cellFont = this.workbook.createFont();
	      cellFont.setStrikeout(true);
	      return cellFont;
	    } 
	    return null;
	  }
	  
	  public HSSFFont colorRojo() {
	    if (this.workbook != null) {
	      HSSFFont cellFont = this.workbook.createFont();
	      cellFont.setColor((short)10);
	      return cellFont;
	    } 
	    return null;
	  }
	  
	  public HSSFFont letraArial() {
	    if (this.workbook != null) {
	      HSSFFont cellFont = this.workbook.createFont();
	      cellFont.setFontName("Arial");
	      return cellFont;
	    } 
	    return null;
	  }
	  
	  public int getnumHojas() {
	    return this.noSheets;
	  }
}
