package com.avon.repNarFronterasAbiertas.reporte;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;



public class ArchivoExcelSXSSF {
	
	private static final Logger logger = LogManager.getLogger(ArchivoExcelSXSSF.class);
	private SXSSFWorkbook workbook;

	private int noSheets = 0;
	  
	  public void leerArchivo(String pathFile) {
		  /*
	    try {
	      this.workbook = null;
	      this.workbook = new SXSSFWorkbook(100);
	      FileInputStream file = new FileInputStream(new File(pathFile));
	      this.workbook = new SXSSFWorkbook(file);
	      this.noSheets = this.workbook.getNumberOfSheets();
	    } catch (FileNotFoundException e) {
	      //loggerError.info("Archivo no encontrado");
	      //loggerError.error("Archivo no encontrado!: " + e);
	      e.printStackTrace();
	    } catch (IOException e) {
	      //loggerError.info("Hubo un error al leer el archivo");
	      //loggerError.error("Error al leer el archivo!: " + e);
	      e.printStackTrace();
	    } 
	    */
		System.out.println("Falta implementar el metodo leerArchivo().");
	  }
	  
	  public void crearArchivoNuevo() {
	    if (this.workbook != null)
	      this.workbook = null; 
	    this.workbook = new SXSSFWorkbook(100);
	  }
	  
	  public Sheet agregarHoja(String nombreHoja) {
	    Sheet hoja = this.workbook.createSheet(nombreHoja);
	    this.noSheets++;
	    return hoja;
	  }
	  
	  public Sheet obtenerHoja(String nombreHoja) {
	    Sheet hoja = this.workbook.getSheet(nombreHoja);
	    return hoja;
	  }
	  
	  public Sheet obtenerHoja(int numHoja) {
	    Sheet hoja = this.workbook.getSheetAt(numHoja);
	    return hoja;
	  }
	  
	  public List<Object> getDatos(int noHoja, int noFila) {
	    List<Object> datos = new ArrayList();
	    Sheet sheet = obtenerHoja(noFila);
	    Row row = sheet.getRow(noFila);
	    Iterator<Cell> cellIter = row.cellIterator();
	    while (cellIter.hasNext()) {
	      Cell cell = (Cell)cellIter.next();
	      switch (cell.getCellType()) {
	        case NUMERIC:
	          if (HSSFDateUtil.isCellDateFormatted((Cell)cell)) {
	            datos.add(cell.getDateCellValue());
	            continue;
	          } 
	          datos.add(Double.valueOf(cell.getNumericCellValue()));
	        case STRING:
	          datos.add(cell.getStringCellValue());
	      } 
	    } 
	    return datos;
	  }
	  
	  public void setDatos(List<Object> listData, int noHoja, int noFila) {
	    Sheet sheet = obtenerHoja(noFila);
	    Row row = sheet.getRow(noFila);
	    Iterator<Object> datIter = listData.iterator();
	    int numcell = 0;
	    while (datIter.hasNext()) {
	      Cell xSSFCell = row.createCell(numcell);
	      Object dato = datIter.next();
	      if (dato instanceof Integer) {
	        xSSFCell.setCellValue(Integer.valueOf((String)dato).intValue());
	      } else if (dato instanceof Float) {
	        xSSFCell.setCellValue(Float.valueOf((String)dato).floatValue());
	      } else if (dato instanceof String) {
	        xSSFCell.setCellValue((String)dato);
	      } else if (dato instanceof Double) {
	        xSSFCell.setCellValue(Double.valueOf((String)dato).doubleValue());
	      } else if (dato instanceof Date) {
	        xSSFCell.setCellValue((Date)dato);
	      } else if (dato instanceof Long) {
	        xSSFCell.setCellValue(Long.valueOf((String)dato).longValue());
	      } else if (dato instanceof Timestamp) {
	        xSSFCell.setCellValue(Timestamp.valueOf((String)dato));
	      } else if (dato != null) {
	        xSSFCell.setCellValue((String)dato);
	      } 
	      numcell++;
	    } 
	  }
	  
	  public void guardarArchivo(String pathFile) {
	    OutputStream salida = null;
	    try {
	      salida = new FileOutputStream(pathFile);
	      if (this.workbook != null) {
	        this.workbook.write(salida);
	        salida.close();
	        this.workbook.dispose(); // Este metodo solo se usa para SXSSF
	      } else {
	        //loggerError.error("No se puede guardar no hay ningun archivo de excel abierto.");
	      } 
	    } catch (FileNotFoundException e) {
	      //loggerError.info("Hubo un error con la ruta a donde guardar");
	      //loggerError.error("Hubo un error con la ruta a donde guardar!: " + e);
	    	StringWriter sw=new StringWriter();
			PrintWriter pw=new PrintWriter(sw);
			e.printStackTrace(pw);
			String sStackTrace=sw.toString();// stack trace as a string
			logger.error(sStackTrace);
	    } catch (IOException e) {
	      //loggerError.info("Hubo un error al guardar el archivo");
	      //loggerError.error("Error al guardar el archivo!: " + e);
	    	StringWriter sw=new StringWriter();
			PrintWriter pw=new PrintWriter(sw);
			e.printStackTrace(pw);
			String sStackTrace=sw.toString();// stack trace as a string
			logger.error(sStackTrace);
	    }
	    catch( Exception e ) {
	    	StringWriter sw=new StringWriter();
			PrintWriter pw=new PrintWriter(sw);
			e.printStackTrace(pw);
			String sStackTrace=sw.toString();// stack trace as a string
			logger.error(sStackTrace);
	    }
	  }
	  
	  public CellStyle estiloBordes() {
	    if (this.workbook != null) {
	      CellStyle xSSFCellStyle = this.workbook.createCellStyle();
	      xSSFCellStyle.setBorderBottom(BorderStyle.HAIR);
	      xSSFCellStyle.setBottomBorderColor((short)8);
	      xSSFCellStyle.setBorderLeft(BorderStyle.HAIR);
	      xSSFCellStyle.setLeftBorderColor((short)8);
	      xSSFCellStyle.setBorderRight(BorderStyle.HAIR);
	      xSSFCellStyle.setRightBorderColor((short)8);
	      xSSFCellStyle.setBorderTop(BorderStyle.HAIR);
	      xSSFCellStyle.setTopBorderColor((short)8);
	      //return (CellStyle)xSSFCellStyle;
	      return xSSFCellStyle;
	    }
	    else {
	    	return null;
	    }
	  }
	  
	  public CellStyle estiloTituloBordes() {
	    if (this.workbook != null) {
	      CellStyle xSSFCellStyle = this.workbook.createCellStyle();
	      xSSFCellStyle.setBorderBottom(BorderStyle.HAIR);
	      xSSFCellStyle.setBottomBorderColor((short)8);
	      xSSFCellStyle.setBorderLeft(BorderStyle.HAIR);
	      xSSFCellStyle.setLeftBorderColor((short)8);
	      xSSFCellStyle.setBorderRight(BorderStyle.HAIR);
	      xSSFCellStyle.setRightBorderColor((short)8);
	      xSSFCellStyle.setBorderTop(BorderStyle.HAIR);
	      xSSFCellStyle.setTopBorderColor((short)8);
	      xSSFCellStyle.setFont(negrita());
	      return xSSFCellStyle;
	    }
	    else {
	    return null;
	    }
	  }
	  
	  public CellStyle estiloTitulos() {
	    if (this.workbook != null) {
	      CellStyle xSSFCellStyle = this.workbook.createCellStyle();
	      xSSFCellStyle.setAlignment(HorizontalAlignment.CENTER);
	      xSSFCellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
	      xSSFCellStyle.setFont(negrita());
	      return xSSFCellStyle;
	    } 
	    return null;
	  }
	  
	  public CellStyle estiloDerecha() {
	    if (this.workbook != null) {
	      CellStyle xSSFCellStyle = this.workbook.createCellStyle();
	      xSSFCellStyle.setAlignment(HorizontalAlignment.CENTER);
	      return xSSFCellStyle;
	    }
	    else {
	    return null;
	    }
	  }
	  
	  public CellStyle estiloIzquierda() {
	    if (this.workbook != null) {
	      CellStyle xSSFCellStyle = this.workbook.createCellStyle();
	      xSSFCellStyle.setAlignment(HorizontalAlignment.LEFT);
	      return xSSFCellStyle;
	    }
	    else {
	    return null;
	    }
	  }
	  
	  public CellStyle estiloCentro() {
	    if (this.workbook != null) {
	      CellStyle xSSFCellStyle = this.workbook.createCellStyle();
	      xSSFCellStyle.setAlignment(HorizontalAlignment.CENTER);
	      return xSSFCellStyle;
	    } 
	    return null;
	  }
	  
	  public Font negrita() {
	    if (this.workbook != null) {
	      Font cellFont = this.workbook.createFont();
	      cellFont.setBold(true);
	      return cellFont;
	    } 
	    return null;
	  }
	  
	  public Font cursiva() {
	    if (this.workbook != null) {
	      Font cellFont = this.workbook.createFont();
	      cellFont.setItalic(true);
	      return cellFont;
	    } 
	    return null;
	  }
	  
	  public Font subrayado() {
	    if (this.workbook != null) {
	      Font cellFont = this.workbook.createFont();
	      cellFont.setStrikeout(true);
	      return cellFont;
	    } 
	    return null;
	  }
	  
	  public Font colorRojo() {
	    if (this.workbook != null) {
	      Font cellFont = this.workbook.createFont();
	      cellFont.setColor((short)10);
	      return cellFont;
	    } 
	    return null;
	  }
	  
	  public Font letraArial() {
	    if (this.workbook != null) {
	      Font cellFont = this.workbook.createFont();
	      cellFont.setFontName("Arial");
	      return cellFont;
	    } 
	    return null;
	  }
	  
	  public int getnumHojas() {
	    return this.noSheets;
	  }
	  
	  public CellStyle estiloBordesDerecha() {
		    if (this.workbook != null) {
		      CellStyle hSSFCellStyle = this.workbook.createCellStyle();
		      hSSFCellStyle.setBorderBottom(BorderStyle.HAIR);
		      hSSFCellStyle.setBottomBorderColor((short)8);
		      hSSFCellStyle.setBorderLeft(BorderStyle.HAIR);
		      hSSFCellStyle.setLeftBorderColor((short)8);
		      hSSFCellStyle.setBorderRight(BorderStyle.HAIR);
		      hSSFCellStyle.setRightBorderColor((short)8);
		      hSSFCellStyle.setBorderTop(BorderStyle.HAIR);
		      hSSFCellStyle.setTopBorderColor((short)8);
		      hSSFCellStyle.setAlignment(HorizontalAlignment.LEFT);
		      return hSSFCellStyle;
		    } 
		    return null;
		  }
}
