package com.avon.repNarFronterasAbiertas.reporte;

import java.awt.Font;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;




public class ArchivoExcel2007 {

	private XSSFWorkbook workbook;
	  
	  private int noSheets = 0;
	  
	  public void leerArchivo(String pathFile) {
	    try {
	      this.workbook = null;
	      FileInputStream file = new FileInputStream(new File(pathFile));
	      this.workbook = new XSSFWorkbook(file);
	      this.noSheets = this.workbook.getNumberOfSheets();
	    } catch (FileNotFoundException e) {
	      //loggerError.info("Archivo no encontrado");
	      //loggerError.error("Archivo no encontrado!: " + e);
	      e.printStackTrace();
	    } catch (IOException e) {
	      //loggerError.info("Hubo un error al leer el archivo");
	      //loggerError.error("Error al leer el archivo!: " + e);
	      e.printStackTrace();
	    } 
	  }
	  
	  public void crearArchivoNuevo() {
	    if (this.workbook != null)
	      this.workbook = null; 
	    this.workbook = new XSSFWorkbook();
	  }
	  
	  public XSSFSheet agregarHoja(String nombreHoja) {
	    XSSFSheet hoja = this.workbook.createSheet(nombreHoja);
	    this.noSheets++;
	    return hoja;
	  }
	  
	  public XSSFSheet obtenerHoja(String nombreHoja) {
	    XSSFSheet hoja = this.workbook.getSheet(nombreHoja);
	    return hoja;
	  }
	  
	  public XSSFSheet obtenerHoja(int numHoja) {
	    XSSFSheet hoja = this.workbook.getSheetAt(numHoja);
	    return hoja;
	  }
	  
	  public List<Object> getDatos(int noHoja, int noFila) {
	    List<Object> datos = new ArrayList();
	    XSSFSheet sheet = obtenerHoja(noFila);
	    XSSFRow row = sheet.getRow(noFila);
	    Iterator<Cell> cellIter = row.cellIterator();
	    while (cellIter.hasNext()) {
	      XSSFCell cell = (XSSFCell)cellIter.next();
	      switch (cell.getCellType()) {
	        case NUMERIC:
	          if (HSSFDateUtil.isCellDateFormatted((Cell)cell)) {
	            datos.add(cell.getDateCellValue());
	            continue;
	          } 
	          datos.add(Double.valueOf(cell.getNumericCellValue()));
	        case STRING:
	          datos.add(cell.getStringCellValue());
	      } 
	    } 
	    return datos;
	  }
	  
	  public void setDatos(List<Object> listData, int noHoja, int noFila) {
	    XSSFSheet sheet = obtenerHoja(noFila);
	    XSSFRow row = sheet.getRow(noFila);
	    Iterator<Object> datIter = listData.iterator();
	    int numcell = 0;
	    while (datIter.hasNext()) {
	      XSSFCell xSSFCell = row.createCell(numcell);
	      Object dato = datIter.next();
	      if (dato instanceof Integer) {
	        xSSFCell.setCellValue(Integer.valueOf((String)dato).intValue());
	      } else if (dato instanceof Float) {
	        xSSFCell.setCellValue(Float.valueOf((String)dato).floatValue());
	      } else if (dato instanceof String) {
	        xSSFCell.setCellValue((String)dato);
	      } else if (dato instanceof Double) {
	        xSSFCell.setCellValue(Double.valueOf((String)dato).doubleValue());
	      } else if (dato instanceof Date) {
	        xSSFCell.setCellValue((Date)dato);
	      } else if (dato instanceof Long) {
	        xSSFCell.setCellValue(Long.valueOf((String)dato).longValue());
	      } else if (dato instanceof Timestamp) {
	        xSSFCell.setCellValue(Timestamp.valueOf((String)dato));
	      } else if (dato != null) {
	        xSSFCell.setCellValue((String)dato);
	      } 
	      numcell++;
	    } 
	  }
	  
	  public void guardarArchivo(String pathFile) {
	    OutputStream salida = null;
	    try {
	      salida = new FileOutputStream(pathFile);
	      if (this.workbook != null) {
	        this.workbook.write(salida);
	        salida.close();
	      } else {
	        //loggerError.error("No se puede guardar no hay ningun archivo de excel abierto.");
	      } 
	    } catch (FileNotFoundException e) {
	      //loggerError.info("Hubo un error con la ruta a donde guardar");
	      //loggerError.error("Hubo un error con la ruta a donde guardar!: " + e);
	      e.printStackTrace();
	    } catch (IOException e) {
	      //loggerError.info("Hubo un error al guardar el archivo");
	      //loggerError.error("Error al guardar el archivo!: " + e);
	      e.printStackTrace();
	    } 
	  }
	  
	  public XSSFCellStyle estiloBordes() {
	    if (this.workbook != null) {
	      XSSFCellStyle xSSFCellStyle = this.workbook.createCellStyle();
	      xSSFCellStyle.setBorderBottom(BorderStyle.HAIR);
	      xSSFCellStyle.setBottomBorderColor((short)8);
	      xSSFCellStyle.setBorderLeft(BorderStyle.HAIR);
	      xSSFCellStyle.setLeftBorderColor((short)8);
	      xSSFCellStyle.setBorderRight(BorderStyle.HAIR);
	      xSSFCellStyle.setRightBorderColor((short)8);
	      xSSFCellStyle.setBorderTop(BorderStyle.HAIR);
	      xSSFCellStyle.setTopBorderColor((short)8);
	      //return (CellStyle)xSSFCellStyle;
	      return xSSFCellStyle;
	    }
	    else {
	    	return null;
	    }
	  }
	  
	  public XSSFCellStyle estiloTituloBordes() {
	    if (this.workbook != null) {
	      XSSFCellStyle xSSFCellStyle = this.workbook.createCellStyle();
	      xSSFCellStyle.setBorderBottom(BorderStyle.HAIR);
	      xSSFCellStyle.setBottomBorderColor((short)8);
	      xSSFCellStyle.setBorderLeft(BorderStyle.HAIR);
	      xSSFCellStyle.setLeftBorderColor((short)8);
	      xSSFCellStyle.setBorderRight(BorderStyle.HAIR);
	      xSSFCellStyle.setRightBorderColor((short)8);
	      xSSFCellStyle.setBorderTop(BorderStyle.HAIR);
	      xSSFCellStyle.setTopBorderColor((short)8);
	      xSSFCellStyle.setFont(negrita());
	      return xSSFCellStyle;
	    }
	    else {
	    return null;
	    }
	  }
	  
	  public XSSFCellStyle estiloTitulos() {
	    if (this.workbook != null) {
	      XSSFCellStyle xSSFCellStyle = this.workbook.createCellStyle();
	      xSSFCellStyle.setAlignment(HorizontalAlignment.CENTER);
	      xSSFCellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
	      xSSFCellStyle.setFont(negrita());
	      return xSSFCellStyle;
	    } 
	    return null;
	  }
	  
	  public XSSFCellStyle estiloDerecha() {
	    if (this.workbook != null) {
	      XSSFCellStyle xSSFCellStyle = this.workbook.createCellStyle();
	      xSSFCellStyle.setAlignment(HorizontalAlignment.CENTER);
	      return xSSFCellStyle;
	    }
	    else {
	    return null;
	    }
	  }
	  
	  public XSSFCellStyle estiloIzquierda() {
	    if (this.workbook != null) {
	      XSSFCellStyle xSSFCellStyle = this.workbook.createCellStyle();
	      xSSFCellStyle.setAlignment(HorizontalAlignment.LEFT);
	      return xSSFCellStyle;
	    }
	    else {
	    return null;
	    }
	  }
	  
	  public XSSFCellStyle estiloCentro() {
	    if (this.workbook != null) {
	      XSSFCellStyle xSSFCellStyle = this.workbook.createCellStyle();
	      xSSFCellStyle.setAlignment(HorizontalAlignment.CENTER);
	      return xSSFCellStyle;
	    } 
	    return null;
	  }
	  
	  public XSSFFont negrita() {
	    if (this.workbook != null) {
	      XSSFFont cellFont = this.workbook.createFont();
	      cellFont.setBold(true);
	      return cellFont;
	    } 
	    return null;
	  }
	  
	  public XSSFFont cursiva() {
	    if (this.workbook != null) {
	      XSSFFont cellFont = this.workbook.createFont();
	      cellFont.setItalic(true);
	      return cellFont;
	    } 
	    return null;
	  }
	  
	  public XSSFFont subrayado() {
	    if (this.workbook != null) {
	      XSSFFont cellFont = this.workbook.createFont();
	      cellFont.setStrikeout(true);
	      return cellFont;
	    } 
	    return null;
	  }
	  
	  public XSSFFont colorRojo() {
	    if (this.workbook != null) {
	      XSSFFont cellFont = this.workbook.createFont();
	      cellFont.setColor((short)10);
	      return cellFont;
	    } 
	    return null;
	  }
	  
	  public XSSFFont letraArial() {
	    if (this.workbook != null) {
	      XSSFFont cellFont = this.workbook.createFont();
	      cellFont.setFontName("Arial");
	      return cellFont;
	    } 
	    return null;
	  }
	  
	  public int getnumHojas() {
	    return this.noSheets;
	  }
	  
	  public XSSFCellStyle estiloBordesDerecha() {
		    if (this.workbook != null) {
		      XSSFCellStyle hSSFCellStyle = this.workbook.createCellStyle();
		      hSSFCellStyle.setBorderBottom(BorderStyle.HAIR);
		      hSSFCellStyle.setBottomBorderColor((short)8);
		      hSSFCellStyle.setBorderLeft(BorderStyle.HAIR);
		      hSSFCellStyle.setLeftBorderColor((short)8);
		      hSSFCellStyle.setBorderRight(BorderStyle.HAIR);
		      hSSFCellStyle.setRightBorderColor((short)8);
		      hSSFCellStyle.setBorderTop(BorderStyle.HAIR);
		      hSSFCellStyle.setTopBorderColor((short)8);
		      hSSFCellStyle.setAlignment(HorizontalAlignment.LEFT);
		      return hSSFCellStyle;
		    } 
		    return null;
		  }
}
