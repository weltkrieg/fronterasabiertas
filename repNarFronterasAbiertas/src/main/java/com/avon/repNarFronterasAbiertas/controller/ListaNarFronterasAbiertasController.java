package com.avon.repNarFronterasAbiertas.controller;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.avon.repNarFronterasAbiertas.dao.ReportesDao;
import com.avon.repNarFronterasAbiertas.dto.FileBean;
import com.avon.repNarFronterasAbiertas.dto.OrdenBean;
import com.avon.repNarFronterasAbiertas.dto.ZonasBean;
import com.avon.repNarFronterasAbiertas.servicio.ReportNarService;
import com.avon.repNarFronterasAbiertas.util.Parametros;
import com.avon.repNarFronterasAbiertas.util.Utilerias;

public class ListaNarFronterasAbiertasController {

	private static final Logger logger = LogManager.getLogger(ListaNarFronterasAbiertasController.class);
	
	private Calendar fecha = Calendar.getInstance();
	private ReportNarService servicio;
	
	public ListaNarFronterasAbiertasController() {
	    this.servicio = new ReportNarService();
	}
	
	public void generarReporteNar() {
	    System.out.println("Consultando...");
	    int anio = this.fecha.get(1);
	    System.out.println("Consultando Zonas..");
	    List<ZonasBean> zonas = ReportesDao.obtenerZonas(anio);
	    System.out.println("Zonas Listas");
	    int campAct = ReportesDao.obtenerCampaniaActual();
	    System.out.println("CampaActual:" + campAct);
	    for (int i = 0; i < zonas.size(); i++) {
	      ZonasBean obj = zonas.get(i);
	      int camp = ReportesDao.obtenerCampania(obj.getAnio(), obj.getZona());
	      int anioCamp = 0;
	      int zonaCamp = 0;
	      System.out.println("Procesando camp: " + camp);
	      if (camp >= campAct - 2) {
	        boolean repartoExist = ReportesDao.revisaExisteReparto(obj.getAnio(), camp, obj.getZona());
	        System.out.println("repartoExist: " + repartoExist);
	        if (repartoExist) {
	          anioCamp = obj.getAnio();
	          zonaCamp = obj.getZona();
	        } else {
	          ArrayList<Integer> campAnt = Utilerias.obtenerCampaniaAnterior(obj.getAnio(), camp);
	          anioCamp = ((Integer)campAnt.get(0)).intValue();
	          camp = ((Integer)campAnt.get(1)).intValue();
	          zonaCamp = obj.getZona();
	        } 
	        System.out.println("Generando Reporte de Zona: " + obj.getZona());
	        //FileBean fbean = this.servicio.generaReporteNarZona(anioCamp, camp, zonaCamp);
	        System.out.println("Reporte Terminado");
	         
	      } 
	    } 
	    
	  }
	
	public void generarReporteFronterasAbiertas() {
		int anioCamp = 0;
		int camp = 0;
		int zonaCamp = 0;
		
		List<OrdenBean> datos = new ArrayList<OrdenBean>();
	    logger.error("Consultando...");
	    int anio = this.fecha.get(1);
	    logger.error("Recuperando Zonas..");
	    List<ZonasBean> zonas = ReportesDao.obtenerZonas(anio);
	    logger.error("Zonas recuperadas exitosamente...");
	    int campAct = ReportesDao.obtenerCampaniaActual();
	    logger.error("Campania actual : " + campAct);
	    for (int i = 0; i < zonas.size(); i++) {
	    
	      ZonasBean obj = zonas.get(i);
	      camp = ReportesDao.obtenerCampania(obj.getAnio(), obj.getZona());
	      anioCamp = 0;
	      zonaCamp = 0;
	      logger.error("Procesando campania: " + camp);
	      if (camp >= campAct - 2) {
	        boolean repartoExist = ReportesDao.revisaExisteReparto(obj.getAnio(), camp, obj.getZona());
	        logger.error("Existe reparto para esta campania " + camp + " y en esta zona " + obj.getZona() + "? : " + repartoExist);
	        if (repartoExist) {
	          anioCamp = obj.getAnio();
	          zonaCamp = obj.getZona();
	        } else {
	          ArrayList<Integer> campAnt = Utilerias.obtenerCampaniaAnterior(obj.getAnio(), camp);
	          anioCamp = ((Integer)campAnt.get(0)).intValue();
	          camp = ((Integer)campAnt.get(1)).intValue();
	          zonaCamp = obj.getZona();
	        }
	        logger.error("Obteniendo info de Zona : " + obj.getZona());
	        datos.addAll(ReportesDao.obtenerOrdenesDevolucion(anioCamp, camp, zonaCamp));
	        //FileBean fbean = this.servicio.generaReporteNarZona(anioCamp, camp, zonaCamp);
	        logger.error("Info recuperada exitosamente...");
	         
	      } 
	    } 
	    
	    logger.error("Total de representantes recuperadas de todas las zonas : " + datos.size());
	    
	    Parametros parametros = new Parametros();
	    String banderaGenerarXlsx = parametros.recuperarPropiedad("generarXLSX");
	    
	    if ( banderaGenerarXlsx != null && banderaGenerarXlsx.trim().length() > 0) {
	    	
	    	if (banderaGenerarXlsx.trim().equalsIgnoreCase("S")) {
		    	//--- CREAR ARCHIVO EXCEL XLSX
			    this.servicio.generarReporteFronterasAbiertas(anioCamp, camp, zonaCamp, datos, campAct);
	    	}
	    	else {
	    		//--- CREAR ARCHIVO EXCEL CSV
			    this.servicio.generarReporteFronterasAbiertasVersionCSV(anioCamp, camp, zonaCamp, datos, campAct);
	    	}
	    }
	    else {
	    	//--- CREAR ARCHIVO EXCEL CSV
		    this.servicio.generarReporteFronterasAbiertasVersionCSV(anioCamp, camp, zonaCamp, datos, campAct);
	    }
	    
	  }
}
