package com.avon.repNarFronterasAbiertas;

import com.avon.repNarFronterasAbiertas.controller.ListaNarFronterasAbiertasController;

public class ListaFronterasAbiertasMain {

	public static void main(String args[]) {
		ListaNarFronterasAbiertasController listaNarFronterasAbiertasController = new ListaNarFronterasAbiertasController();
		listaNarFronterasAbiertasController.generarReporteFronterasAbiertas();
	}
}
