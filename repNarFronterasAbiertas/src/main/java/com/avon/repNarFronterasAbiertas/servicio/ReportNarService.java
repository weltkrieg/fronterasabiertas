package com.avon.repNarFronterasAbiertas.servicio;

import java.io.FileWriter;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;

import com.avon.repNarFronterasAbiertas.dao.ReportesDao;
import com.avon.repNarFronterasAbiertas.dto.FileBean;
import com.avon.repNarFronterasAbiertas.dto.OrdenBean;
import com.avon.repNarFronterasAbiertas.reporte.ArchivoExcel2007;
import com.avon.repNarFronterasAbiertas.reporte.ArchivoExcelSXSSF;
import com.avon.repNarFronterasAbiertas.util.Parametros;
import com.avon.repNarFronterasAbiertas.util.Utilerias;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ReportNarService {

	private static final Logger logger = LogManager.getLogger(ReportNarService.class);
	
	int cantidadOrdenesDevolucion;
	  
	  int cantidadTotalOrdenes;
	  
	  float tot;
	  
	  float sumaTotalOrdenesDevolucion;
	  
	  int totalOrdenesLibres;
	  
	  int totalOrdenesLibresNoEnt;
	  
	  float sumaTotalOrdenesLibres;
	  
	  int cantidadOrdenesDevueltas;
	  
	  float sumaTotOrdenesDevueltas;
	  
	  public void obtenerDatosReporte(int anio, int campania, int zona) {
	    this.cantidadOrdenesDevolucion = ReportesDao.obtenerCantidadOrdenesDevolucion(anio, campania, zona);
	    this.cantidadTotalOrdenes = ReportesDao.obtenerCantidadTotalOrdenes(anio, campania, zona);
	    this.tot = ReportesDao.obtenerSumaTotalOrdenesDevolucion(anio, campania, zona);
	    this.sumaTotalOrdenesDevolucion = ReportesDao.obtenerSumaTotalOrdenesDevolucion(anio, campania, zona);
	    this.totalOrdenesLibres = ReportesDao.obtenerTotalOrdenesLibres(anio, campania, zona);
	    this.totalOrdenesLibresNoEnt = ReportesDao.obtenerTotalOrdenesLibresNoEntregadas(anio, campania, zona);
	    this.sumaTotalOrdenesLibres = ReportesDao.obtenerSumaTotalOrdenesLibres(anio, campania, zona);
	    this.cantidadOrdenesDevueltas = ReportesDao.obtenerCantidadOrdenesDevueltas(anio, campania, zona);
	    this.sumaTotOrdenesDevueltas = ReportesDao.obtenerSumaTotalOrdenesDevueltas(anio, campania, zona);
	  }
	  
	  public FileBean generarReporteFronterasAbiertasVersionCSV(int anio, int campania, int zona, List<OrdenBean> datos, int campaniaActual) {
		    FileBean fbean = new FileBean();
		    Parametros parametros = new Parametros();
		    String campaniaActualAux = String.format("%02d", campaniaActual);
		    DecimalFormat df = new DecimalFormat("#.##");
		    String pattern = "ddMMyyyy";
		    SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
		    String fechaCreacion = simpleDateFormat.format(new Date());
		    FileWriter writer = null;
		    String lineaGuardar = "";
		    
		    
		    try {
		      logger.error("Generando Archivo......");
		      String dirArchExcelGenerar = parametros.recuperarPropiedad("dirArchivoExcel");
		      String nomArchExcelGenerar = parametros.recuperarPropiedad("nomArchivoExcel");
		      String nomDirCompleto = dirArchExcelGenerar + "/" + nomArchExcelGenerar + "_C" + campaniaActualAux + "_" + fechaCreacion + ".csv";
		      
		      writer = new FileWriter(nomDirCompleto);
		      
		      writer.write(",Lista Fronteras Abiertas\n");
		      writer.write(",\n");
		      writer.write("REGISTRO,NOMBRE,CALLE NUMERO COLONIA,ENTRE CALLES,POBLACION,VALOR COD,VALOR PEDIDO,RED,CAJAS,CAUSA DE DEVOLUCION,BALANCE\n");
		      
		      for ( OrdenBean orden : datos) {
		    	  
		    	  lineaGuardar = Utilerias.completar(8, orden.getRegistro().intValue()) + ",";
		    	  
		    	  if (orden.getNombre() != null && orden.getNombre().trim().length() > 0) {
		    		  lineaGuardar += orden.getNombre().trim() + ",";			     
			      }
		    	  else {
		    		  lineaGuardar += ",";
			      }
		    	  
		    	  if (orden.getDireccion() != null && orden.getDireccion().trim().length() > 0) {
		    		  lineaGuardar += orden.getDireccion().trim() + ",";
			      }
		    	  else {
		    		  lineaGuardar += ",";
			      }
		    	  
		    	  if (orden.getCalles() != null && orden.getCalles().trim().length() > 0) {
		    		  lineaGuardar += orden.getCalles().trim() + ",";
			      }
		    	  else {
		    		  lineaGuardar += ",";
			      }
		    	  
		    	  if (orden.getPoblacion() != null && orden.getPoblacion().trim().length() > 0) {
		    		  lineaGuardar += orden.getPoblacion().trim() + ",";
			      }
		    	  else {
		    		  lineaGuardar += ",";
			      }
		    	  		    	  
		          lineaGuardar += df.format(orden.getValorCod()) + ",";
		          
		          lineaGuardar += df.format(orden.getValorPedido()) + ",";
		          
		          lineaGuardar += orden.getRed() + ",";
		          
		          lineaGuardar += orden.getCajas() + ",";
		          
		          if (orden.getCausa() != null && orden.getCausa().trim().length() > 0) {
		    		  lineaGuardar += orden.getCausa().trim() + ",";
			      }
		    	  else {
		    		  lineaGuardar += ",";
			      }
		          
		          
		          lineaGuardar += df.format(orden.getBalance());
		          
		          writer.write(lineaGuardar +"\n");
		          
		      }
		      
		      writer.flush();
		      writer.close();
		      		      
		      
		      fbean.setFileName(nomDirCompleto);
		      fbean.setAnio(anio);
		      fbean.setCampania(campania);
		      fbean.setZona(zona);
		      logger.error("Archivo Excel generado: " + nomDirCompleto);
		      return fbean;
		    } catch (Exception e) {
		      e.printStackTrace();
		      //System.out.println("Exception:" + e);
		      logger.error("Exception:" + e);
		      return null;
		    } 
		  }
	  
	  public FileBean generarReporteFronterasAbiertas(int anio, int campania, int zona, List<OrdenBean> datos, int campaniaActual) {
	    FileBean fbean = new FileBean();
	    Parametros parametros = new Parametros();
	    ArchivoExcelSXSSF libro = new ArchivoExcelSXSSF();
	    String campaniaActualAux = String.format("%02d", campaniaActual);
	    String pattern = "ddMMyyyy";
	    SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
	    String fechaCreacion = simpleDateFormat.format(new Date());
	    
	    
	    try {
	      libro.crearArchivoNuevo();
	      //obtenerDatosReporte(anio, campania, zona);
	      //System.out.println("Se esta generando Hoja 1");
	      logger.error("Se esta generando Hoja 1");
	      creaHoja1Zona(libro, anio, campania, zona, datos);
	      /*
	      System.out.println("Se esta generando Hoja 2");
	      creaHoja2Zona(libro, anio, campania, zona);
	      System.out.println("Se esta generando Hoja 3");
	      creaHoja3Zona(libro, anio, campania, zona);
	      System.out.println("Se esta generando Hoja 4");
	      creaHoja4Zona(libro, anio, campania, zona);
	      */
	      //System.out.println("Se va a guardar libro");
	      logger.error("Se va a guardar libro");
	      
	      String dirArchExcelGenerar = parametros.recuperarPropiedad("dirArchivoExcel");
	      String nomArchExcelGenerar = parametros.recuperarPropiedad("nomArchivoExcel");
	      String nomDirCompleto = dirArchExcelGenerar + "/" + nomArchExcelGenerar + "_C" + campaniaActualAux + "_" + fechaCreacion + ".xlsx";
	      
	      //String dirArchExcelGenerar = "/Apps/ftpdir/fronterasAbiertas/fronteraAbierta.xlsx";
	      //String nomDirCompleto = "/Apps/scheduler/Apps/OTSTools/fronterasAbiertasV2" + "/" + "fronteraAbierta" + "_C" + campaniaActualAux + "_" + fechaCreacion + ".xlsx";
	      fbean.setFileName(nomDirCompleto);
	      fbean.setAnio(anio);
	      fbean.setCampania(campania);
	      fbean.setZona(zona);
	      libro.guardarArchivo(nomDirCompleto);
	      //System.out.println("\nZona: " + zona);
	      //System.out.println("Archivo generado: " + dirArchExcelGenerar);
	      logger.error("Archivo Excel generado: " + nomDirCompleto);
	      return fbean;
	    } catch (Exception e) {
	      e.printStackTrace();
	      //System.out.println("Exception:" + e);
	      logger.error("Exception:" + e);
	      return null;
	    } 
	  }
	  
	  public Sheet creaHoja1Zona(ArchivoExcelSXSSF libro, int anio, int campania, int zona, List<OrdenBean> datos) {
		  DecimalFormat df = new DecimalFormat("#.##");
		    try {
		      logger.error("Entrando metodo creaHoja1Zona");
		      CellStyle estiloBordes = libro.estiloBordes();
		      
		      CellStyle estiloBordesTitulo = libro.estiloTituloBordes();
		      
		      CellStyle estiloDerecha = libro.estiloDerecha();
		      
		      CellStyle estiloBorderDer = libro.estiloBordesDerecha();
		      
		      Sheet hoja = libro.agregarHoja("Lista Fronteras Abiertas");
		      
		      hoja.setDisplayGridlines(false);
		      Row linea1 = hoja.createRow(0);
		      Cell celda11 = linea1.createCell(2);
		      celda11.setCellValue("LISTA DE FRONTERAS ABIERTAS");
		      celda11.setCellStyle(libro.estiloTitulos());
		      /*
		      HSSFRow linea2 = hoja.createRow(1);
		      HSSFCell celda21 = linea2.createCell(0);
		      celda21.setCellStyle(estiloBordesTitulo);
		      celda21.setCellValue("ZONA");
		      HSSFCell celda22 = linea2.createCell(1);
		      celda22.setCellStyle(estiloBorderDer);
		      celda22.setCellValue(zona);
		      HSSFRow linea3 = hoja.createRow(2);
		      HSSFCell celda31 = linea3.createCell(0);
		      celda31.setCellStyle(estiloBordesTitulo);
		      celda31.setCellValue("CAMPAÑA");
		      HSSFCell celda32 = linea3.createCell(1);
		      celda32.setCellStyle(estiloBorderDer);
		      celda32.setCellValue("00000-0" + String.valueOf(campania));
		      HSSFCell celda33 = linea3.createCell(5);
		      celda33.setCellStyle(estiloBordesTitulo);
		      celda33.setCellValue("% ACEPTACION");
		      HSSFCell celda34 = linea3.createCell(6);
		      celda34.setCellStyle(estiloBordesTitulo);
		      celda34.setCellValue("MI VENTA NO ACEPTADA EN REPARTO");
		      HSSFRow linea4 = hoja.createRow(3);
		      HSSFCell celda41 = linea4.createCell(0);
		      celda41.setCellStyle(estiloBordesTitulo);
		      celda41.setCellValue("ORDENES CON NO ACEPTACION DE REPARTO");
		      HSSFCell celda42 = linea4.createCell(1);
		      celda42.setCellStyle(estiloBorderDer);
		      celda42.setCellValue(this.cantidadOrdenesDevolucion);
		      float a = 0.0F, b = 0.0F, c = 0.0F;
		      a = this.cantidadTotalOrdenes;
		      b = this.cantidadOrdenesDevolucion;
		      c = (a - b) / a * 100.0F;
		      DecimalFormat df = new DecimalFormat("#.##");
		      HSSFCell celda43 = linea4.createCell(5);
		      celda43.setCellStyle(estiloBorderDer);
		      celda43.setCellType(CellType.NUMERIC);
		      celda43.setCellValue(df.format(c) + "%");
		      HSSFCell celda44 = linea4.createCell(6);
		      celda44.setCellStyle(estiloBorderDer);
		      celda44.setCellValue(Double.valueOf(df.format(this.tot)).doubleValue());
		      HSSFRow linea5 = hoja.createRow(4);
		      HSSFCell celda51 = linea5.createCell(0);
		      celda51.setCellStyle(estiloBordesTitulo);
		      celda51.setCellValue("TOTAL ORDENES");
		      HSSFCell celda52 = linea5.createCell(1);
		      celda52.setCellValue(this.cantidadTotalOrdenes);
		      celda52.setCellStyle(estiloBorderDer);
		      */
		      Row linea6 = hoja.createRow(1);
		      Cell celda61 = linea6.createCell(0);
		      celda61.setCellValue("");
		      Row titulo = hoja.createRow(2);
		      Cell registro = titulo.createCell(0);
		      registro.setCellValue("REGISTRO");
		      registro.setCellStyle(estiloBordesTitulo);
		      Cell nombre = titulo.createCell(1);
		      nombre.setCellValue("NOMBRE");
		      nombre.setCellStyle(estiloBordesTitulo);
		      Cell direccion = titulo.createCell(2);
		      direccion.setCellValue("CALLE, NUMERO, COLONIA");
		      direccion.setCellStyle(estiloBordesTitulo);
		      Cell calles = titulo.createCell(3);
		      calles.setCellValue("ENTRE CALLES");
		      calles.setCellStyle(estiloBordesTitulo);
		      Cell poblacion = titulo.createCell(4);
		      poblacion.setCellValue("POBLACION");
		      poblacion.setCellStyle(estiloBordesTitulo);
		      Cell valorCod = titulo.createCell(5);
		      valorCod.setCellValue("VALOR COD");
		      valorCod.setCellStyle(estiloBordesTitulo);
		      Cell valorPedido = titulo.createCell(6);
		      valorPedido.setCellValue("VALOR PEDIDO");
		      valorPedido.setCellStyle(estiloBordesTitulo);
		      Cell red = titulo.createCell(7);
		      red.setCellValue("RED");
		      red.setCellStyle(estiloBordesTitulo);
		      Cell cajas = titulo.createCell(8);
		      cajas.setCellValue("CAJAS");
		      cajas.setCellStyle(estiloBordesTitulo);
		      Cell causa = titulo.createCell(9);
		      causa.setCellValue("CAUSA DE DEVOLUCION");
		      causa.setCellStyle(estiloBordesTitulo);
		      Cell balance = titulo.createCell(10);
		      balance.setCellValue("BALANCE");
		      balance.setCellStyle(estiloBordesTitulo);
		      //List<OrdenBean> datos = ReportesDao.obtenerOrdenesDevolucion(anio, campania, zona);
		      int s = 3;
		      
		      for (int k = 0; k < datos.size(); k++) {
		        Row fila = hoja.createRow(s);
		        
		        for (int i = 0; i < 10; i++) {
		        	
		          OrdenBean obj = datos.get(k);
		          Cell registrod = fila.createCell(0);
		          registrod.setCellStyle(estiloBordes);
		          registrod.setCellValue(Utilerias.completar(8, obj.getRegistro().intValue()));
		          Cell nombred = fila.createCell(1);
		          nombred.setCellStyle(estiloBordes);
		          if (obj.getNombre() == null) {
		            nombred.setCellValue("");
		          } else {
		            nombred.setCellValue(obj.getNombre().toString());
		          } 
		          Cell direcciond = fila.createCell(2);
		          direcciond.setCellStyle(estiloBordes);
		          if (obj.getDireccion() == null) {
		            direcciond.setCellValue("");
		          } else {
		            direcciond.setCellValue(obj.getDireccion().toString());
		          } 
		          Cell callesd = fila.createCell(3);
		          callesd.setCellStyle(estiloBordes);
		          if (obj.getCalles() == null) {
		            callesd.setCellValue("");
		          } else {
		            callesd.setCellValue(obj.getCalles().toString());
		          } 
		          Cell poblaciond = fila.createCell(4);
		          poblaciond.setCellStyle(estiloBordes);
		          if (obj.getPoblacion() == null) {
		            poblaciond.setCellValue("");
		          } else {
		            poblaciond.setCellValue(obj.getPoblacion().toString());
		          } 
		          Cell valorCodd = fila.createCell(5);
		          valorCodd.setCellStyle(estiloBordes);
		          valorCodd.setCellValue(Double.valueOf(df.format(obj.getValorCod())).doubleValue());
		          Cell valorPedidod = fila.createCell(6);
		          valorPedidod.setCellStyle(estiloBordes);
		          valorPedidod.setCellValue(Double.valueOf(df.format(obj.getValorPedido())).doubleValue());
		          Cell redd = fila.createCell(7);
		          redd.setCellStyle(estiloBordes);
		          redd.setCellValue(obj.getRed());
		          Cell cajasd = fila.createCell(8);
		          cajasd.setCellStyle(estiloBordes);
		          cajasd.setCellValue(obj.getCajas().intValue());
		          Cell causad = fila.createCell(9);
		          causad.setCellStyle(estiloBordes);
		          if (obj.getCausa() == null) {
		            causad.setCellValue("");
		          } else {
		            causad.setCellValue(obj.getCausa().toString());
		          } 
		          Cell balanced = fila.createCell(10);
		          balanced.setCellStyle(estiloBordes);
		          balanced.setCellValue(Double.valueOf(df.format(obj.getBalance())).doubleValue());
		        } 
		        s++;
		      } 
		      int celdaFinal = this.cantidadOrdenesDevolucion + 3;
		      hoja.setAutoFilter(CellRangeAddress.valueOf("A3:K" + celdaFinal));
		      
		      /*
		      hoja.autoSizeColumn(0);
		      hoja.autoSizeColumn(1);
		      hoja.autoSizeColumn(2);
		      hoja.autoSizeColumn(3);
		      hoja.autoSizeColumn(4);
		      hoja.autoSizeColumn(5);
		      hoja.autoSizeColumn(6);
		      hoja.autoSizeColumn(7);
		      hoja.autoSizeColumn(8);
		      hoja.autoSizeColumn(9);
		      hoja.autoSizeColumn(10);
		      */
		      return hoja;
		    } catch (Exception e) {
		      //System.out.println("Ocurrio un error al generar el libro! - " + e.getMessage() );
		      logger.error("Ocurrio un error al generar el libro! - " + e.getMessage());
		      StringWriter sw = new StringWriter();
		      e.printStackTrace(new PrintWriter(sw));
		      String exceptionAsString = sw.toString();
		      //System.out.println("Exception 2 - " + exceptionAsString );
		      logger.error("Exception 2 - " + exceptionAsString);
		      //loggerError.error("Exception", e);
		      return null;
		    } 
		  }
	  /*
	  public HSSFSheet creaHoja1Zona(ArchivoExcel2003 libro, int anio, int campania, int zona) {
	    try {
	      CellStyle estiloBordes = libro.estiloBordes();
	      CellStyle estiloBordesTitulo = libro.estiloTituloBordes();
	      CellStyle estiloDerecha = libro.estiloDerecha();
	      CellStyle estiloBorderDer = libro.estiloBordesDerecha();
	      HSSFSheet hoja = libro.agregarHoja("Lista NAR");
	      hoja.setDisplayGridlines(false);
	      HSSFRow linea1 = hoja.createRow(0);
	      HSSFCell celda11 = linea1.createCell(2);
	      celda11.setCellValue("LISTA DE NO ACEPTACION EN REPARTO");
	      celda11.setCellStyle(libro.estiloTitulos());
	      HSSFRow linea2 = hoja.createRow(1);
	      HSSFCell celda21 = linea2.createCell(0);
	      celda21.setCellStyle(estiloBordesTitulo);
	      celda21.setCellValue("ZONA");
	      HSSFCell celda22 = linea2.createCell(1);
	      celda22.setCellStyle(estiloBorderDer);
	      celda22.setCellValue(zona);
	      HSSFRow linea3 = hoja.createRow(2);
	      HSSFCell celda31 = linea3.createCell(0);
	      celda31.setCellStyle(estiloBordesTitulo);
	      celda31.setCellValue("CAMPAÑA");
	      HSSFCell celda32 = linea3.createCell(1);
	      celda32.setCellStyle(estiloBorderDer);
	      celda32.setCellValue("00000-0" + String.valueOf(campania));
	      HSSFCell celda33 = linea3.createCell(5);
	      celda33.setCellStyle(estiloBordesTitulo);
	      celda33.setCellValue("% ACEPTACION");
	      HSSFCell celda34 = linea3.createCell(6);
	      celda34.setCellStyle(estiloBordesTitulo);
	      celda34.setCellValue("MI VENTA NO ACEPTADA EN REPARTO");
	      HSSFRow linea4 = hoja.createRow(3);
	      HSSFCell celda41 = linea4.createCell(0);
	      celda41.setCellStyle(estiloBordesTitulo);
	      celda41.setCellValue("ORDENES CON NO ACEPTACION DE REPARTO");
	      HSSFCell celda42 = linea4.createCell(1);
	      celda42.setCellStyle(estiloBorderDer);
	      celda42.setCellValue(this.cantidadOrdenesDevolucion);
	      float a = 0.0F, b = 0.0F, c = 0.0F;
	      a = this.cantidadTotalOrdenes;
	      b = this.cantidadOrdenesDevolucion;
	      c = (a - b) / a * 100.0F;
	      DecimalFormat df = new DecimalFormat("#.##");
	      HSSFCell celda43 = linea4.createCell(5);
	      celda43.setCellStyle(estiloBorderDer);
	      celda43.setCellType(CellType.NUMERIC);
	      celda43.setCellValue(df.format(c) + "%");
	      HSSFCell celda44 = linea4.createCell(6);
	      celda44.setCellStyle(estiloBorderDer);
	      celda44.setCellValue(Double.valueOf(df.format(this.tot)).doubleValue());
	      HSSFRow linea5 = hoja.createRow(4);
	      HSSFCell celda51 = linea5.createCell(0);
	      celda51.setCellStyle(estiloBordesTitulo);
	      celda51.setCellValue("TOTAL ORDENES");
	      HSSFCell celda52 = linea5.createCell(1);
	      celda52.setCellValue(this.cantidadTotalOrdenes);
	      celda52.setCellStyle(estiloBorderDer);
	      HSSFRow linea6 = hoja.createRow(5);
	      HSSFCell celda61 = linea6.createCell(0);
	      celda61.setCellValue("");
	      HSSFRow titulo = hoja.createRow(6);
	      HSSFCell registro = titulo.createCell(0);
	      registro.setCellValue("REGISTRO");
	      registro.setCellStyle(estiloBordesTitulo);
	      HSSFCell nombre = titulo.createCell(1);
	      nombre.setCellValue("NOMBRE");
	      nombre.setCellStyle(estiloBordesTitulo);
	      HSSFCell direccion = titulo.createCell(2);
	      direccion.setCellValue("CALLE, NUMERO, COLONIA");
	      direccion.setCellStyle(estiloBordesTitulo);
	      HSSFCell calles = titulo.createCell(3);
	      calles.setCellValue("ENTRE CALLES");
	      calles.setCellStyle(estiloBordesTitulo);
	      HSSFCell poblacion = titulo.createCell(4);
	      poblacion.setCellValue("POBLACION");
	      poblacion.setCellStyle(estiloBordesTitulo);
	      HSSFCell valorCod = titulo.createCell(5);
	      valorCod.setCellValue("VALOR COD");
	      valorCod.setCellStyle(estiloBordesTitulo);
	      HSSFCell valorPedido = titulo.createCell(6);
	      valorPedido.setCellValue("VALOR PEDIDO");
	      valorPedido.setCellStyle(estiloBordesTitulo);
	      HSSFCell red = titulo.createCell(7);
	      red.setCellValue("RED");
	      red.setCellStyle(estiloBordesTitulo);
	      HSSFCell cajas = titulo.createCell(8);
	      cajas.setCellValue("CAJAS");
	      cajas.setCellStyle(estiloBordesTitulo);
	      HSSFCell causa = titulo.createCell(9);
	      causa.setCellValue("CAUSA DE DEVOLUCION");
	      causa.setCellStyle(estiloBordesTitulo);
	      HSSFCell balance = titulo.createCell(10);
	      balance.setCellValue("BALANCE");
	      balance.setCellStyle(estiloBordesTitulo);
	      List<OrdenBean> datos = ReportesDao.obtenerOrdenesDevolucion(anio, campania, zona);
	      int s = 7;
	      for (int k = 0; k < datos.size(); k++) {
	        HSSFRow fila = hoja.createRow(s);
	        for (int i = 0; i < 10; i++) {
	          OrdenBean obj = datos.get(k);
	          HSSFCell registrod = fila.createCell(0);
	          registrod.setCellStyle(estiloBordes);
	          registrod.setCellValue(Utilerias.completar(8, obj.getRegistro().intValue()));
	          HSSFCell nombred = fila.createCell(1);
	          nombred.setCellStyle(estiloBordes);
	          if (obj.getNombre() == null) {
	            nombred.setCellValue("");
	          } else {
	            nombred.setCellValue(obj.getNombre().toString());
	          } 
	          HSSFCell direcciond = fila.createCell(2);
	          direcciond.setCellStyle(estiloBordes);
	          if (obj.getDireccion() == null) {
	            direcciond.setCellValue("");
	          } else {
	            direcciond.setCellValue(obj.getDireccion().toString());
	          } 
	          HSSFCell callesd = fila.createCell(3);
	          callesd.setCellStyle(estiloBordes);
	          if (obj.getCalles() == null) {
	            callesd.setCellValue("");
	          } else {
	            callesd.setCellValue(obj.getCalles().toString());
	          } 
	          HSSFCell poblaciond = fila.createCell(4);
	          poblaciond.setCellStyle(estiloBordes);
	          if (obj.getPoblacion() == null) {
	            poblaciond.setCellValue("");
	          } else {
	            poblaciond.setCellValue(obj.getPoblacion().toString());
	          } 
	          HSSFCell valorCodd = fila.createCell(5);
	          valorCodd.setCellStyle(estiloBordes);
	          valorCodd.setCellValue(Double.valueOf(df.format(obj.getValorCod())).doubleValue());
	          HSSFCell valorPedidod = fila.createCell(6);
	          valorPedidod.setCellStyle(estiloBordes);
	          valorPedidod.setCellValue(Double.valueOf(df.format(obj.getValorPedido())).doubleValue());
	          HSSFCell redd = fila.createCell(7);
	          redd.setCellStyle(estiloBordes);
	          redd.setCellValue(obj.getRed());
	          HSSFCell cajasd = fila.createCell(8);
	          cajasd.setCellStyle(estiloBordes);
	          cajasd.setCellValue(obj.getCajas().intValue());
	          HSSFCell causad = fila.createCell(9);
	          causad.setCellStyle(estiloBordes);
	          if (obj.getCausa() == null) {
	            causad.setCellValue("");
	          } else {
	            causad.setCellValue(obj.getCausa().toString());
	          } 
	          HSSFCell balanced = fila.createCell(10);
	          balanced.setCellStyle(estiloBordes);
	          balanced.setCellValue(Double.valueOf(df.format(obj.getBalance())).doubleValue());
	        } 
	        s++;
	      } 
	      int celdaFinal = this.cantidadOrdenesDevolucion + 7;
	      hoja.setAutoFilter(CellRangeAddress.valueOf("A7:K" + celdaFinal));
	      hoja.autoSizeColumn(0);
	      hoja.autoSizeColumn(1);
	      hoja.autoSizeColumn(2);
	      hoja.autoSizeColumn(3);
	      hoja.autoSizeColumn(4);
	      hoja.autoSizeColumn(5);
	      hoja.autoSizeColumn(6);
	      hoja.autoSizeColumn(7);
	      hoja.autoSizeColumn(8);
	      hoja.autoSizeColumn(9);
	      hoja.autoSizeColumn(10);
	      return hoja;
	    } catch (Exception e) {
	      System.out.println("Ocurrio un error al generar el libro!");
	      //loggerError.error("Exception", e);
	      return null;
	    } 
	  }
	  */
	  /*
	  public HSSFSheet creaHoja2Zona(ArchivoExcel2003 libro, int anio, int campania, int zona) {
	    CellStyle estiloBordes = libro.estiloBordes();
	    CellStyle estiloBordesTitulo = libro.estiloTituloBordes();
	    try {
	      HSSFSheet hoja2 = libro.agregarHoja("Total Orders");
	      hoja2.setDisplayGridlines(false);
	      HSSFRow titulo = hoja2.createRow(0);
	      HSSFCell celda11 = titulo.createCell(0);
	      celda11.setCellStyle(estiloBordesTitulo);
	      celda11.setCellValue("Registro");
	      HSSFCell celda12 = titulo.createCell(1);
	      celda12.setCellStyle(estiloBordesTitulo);
	      celda12.setCellValue("Red");
	      HSSFCell celda13 = titulo.createCell(2);
	      celda13.setCellStyle(estiloBordesTitulo);
	      celda13.setCellValue("Valor COD");
	      int s = 1;
	      DecimalFormat df = new DecimalFormat("#.##");
	      List<OrdenZonaBean> datos = ReportesDao.obtenerOrdenesRed(anio, campania, zona);
	      for (int i = 0; i < datos.size(); i++) {
	        HSSFRow linea = hoja2.createRow(s);
	        for (int j = 0; j < 3; j++) {
	          OrdenZonaBean obj = datos.get(i);
	          HSSFCell registro = linea.createCell(0);
	          registro.setCellStyle(estiloBordes);
	          registro.setCellValue(Utilerias.completar(8, obj.getRegistro()));
	          HSSFCell red = linea.createCell(1);
	          red.setCellStyle(estiloBordes);
	          red.setCellValue(obj.getRed());
	          HSSFCell valorcod = linea.createCell(2);
	          valorcod.setCellStyle(estiloBordes);
	          valorcod.setCellValue(Double.valueOf(df.format(obj.getValorCod())).doubleValue());
	        } 
	        s++;
	      } 
	      int celdaFinal = this.cantidadTotalOrdenes + 1;
	      hoja2.setAutoFilter(CellRangeAddress.valueOf("A1:C" + celdaFinal));
	      hoja2.autoSizeColumn(0);
	      hoja2.autoSizeColumn(1);
	      hoja2.autoSizeColumn(2);
	      return hoja2;
	    } catch (Exception e) {
	      System.out.println("Ocurrio un error al crear la hoja!");
	      loggerError.error("Exception", e);
	      return null;
	    } 
	  }
	  
	  public HSSFSheet creaHoja3Zona(ArchivoExcel2003 libro, int anio, int campania, int zona) {
	    CellStyle estiloBordes = libro.estiloBordes();
	    CellStyle estiloBordesTitulo = libro.estiloTituloBordes();
	    try {
	      HSSFSheet hoja3 = libro.agregarHoja("Totales por Red");
	      hoja3.setDisplayGridlines(false);
	      HSSFRow linea1 = hoja3.createRow(1);
	      HSSFCell celda12 = linea1.createCell(1);
	      celda12.setCellStyle(estiloBordesTitulo);
	      celda12.setCellValue("% Aceptacion Zona");
	      HSSFCell celda13 = linea1.createCell(2);
	      celda13.setCellStyle(estiloBordesTitulo);
	      celda13.setCellValue("Cantidad de Ordenes");
	      HSSFCell celda14 = linea1.createCell(3);
	      celda14.setCellStyle(estiloBordesTitulo);
	      celda14.setCellValue("Importe Total");
	      HSSFCell celda17 = linea1.createCell(6);
	      celda17.setCellStyle(estiloBordesTitulo);
	      celda17.setCellValue("# Ordenes Libres de Cobro no entregadas");
	      HSSFCell celda18 = linea1.createCell(7);
	      celda18.setCellStyle(estiloBordesTitulo);
	      celda18.setCellValue("% Aceptacion Libres de Cobro Zona");
	      float a = 0.0F, b = 0.0F, c = 0.0F;
	      a = this.cantidadTotalOrdenes;
	      b = this.cantidadOrdenesDevolucion;
	      c = (a - b) / a * 100.0F;
	      DecimalFormat df = new DecimalFormat("#.##");
	      HSSFRow linea2 = hoja3.createRow(2);
	      HSSFCell celda22 = linea2.createCell(1);
	      celda22.setCellStyle(estiloBordes);
	      celda22.setCellValue(df.format(c) + "%");
	      HSSFCell celda23 = linea2.createCell(2);
	      celda23.setCellStyle(estiloBordes);
	      celda23.setCellValue(this.cantidadOrdenesDevolucion);
	      HSSFCell celda24 = linea2.createCell(3);
	      celda24.setCellStyle(estiloBordes);
	      celda24.setCellValue(Double.valueOf(df.format(this.sumaTotalOrdenesDevolucion)).doubleValue());
	      HSSFCell celda25 = linea2.createCell(4);
	      celda25.setCellValue(this.cantidadTotalOrdenes);
	      HSSFCell celda27 = linea2.createCell(6);
	      celda27.setCellStyle(estiloBordes);
	      celda27.setCellValue(this.totalOrdenesLibresNoEnt);
	      float d = 0.0F, e = 0.0F, f = 0.0F;
	      d = this.totalOrdenesLibres;
	      e = this.totalOrdenesLibresNoEnt;
	      f = (d - e) / d * 100.0F;
	      HSSFCell celda28 = linea2.createCell(7);
	      celda28.setCellStyle(estiloBordes);
	      celda28.setCellValue(df.format(f) + "%");
	      HSSFRow linea3 = hoja3.createRow(3);
	      HSSFCell linea37 = linea3.createCell(7);
	      linea37.setCellValue(this.sumaTotalOrdenesLibres);
	      HSSFCell celda38 = linea3.createCell(8);
	      celda38.setCellValue(this.totalOrdenesLibres);
	      HSSFRow titulo = hoja3.createRow(4);
	      HSSFCell red = titulo.createCell(0);
	      red.setCellValue("Red");
	      red.setCellStyle(estiloBordesTitulo);
	      HSSFCell aceptacion = titulo.createCell(1);
	      aceptacion.setCellValue("% Aceptacion Red");
	      aceptacion.setCellStyle(estiloBordesTitulo);
	      HSSFCell ordenesdev = titulo.createCell(2);
	      ordenesdev.setCellValue("# Ordenes Devolucion");
	      ordenesdev.setCellStyle(estiloBordesTitulo);
	      HSSFCell sumatoria = titulo.createCell(3);
	      sumatoria.setCellValue("Sumatoria Valor Pedido por Red");
	      sumatoria.setCellStyle(estiloBordesTitulo);
	      HSSFCell total = titulo.createCell(4);
	      total.setCellValue("Total Ordenes");
	      total.setCellStyle(estiloBordesTitulo);
	      HSSFCell ordeneslib = titulo.createCell(6);
	      ordeneslib.setCellValue("# Ordenes Libres de cobro no entregadas");
	      ordeneslib.setCellStyle(estiloBordesTitulo);
	      HSSFCell sumatorialib = titulo.createCell(7);
	      sumatorialib.setCellValue("Sumatoria Valor Pedido Libres de Cobro");
	      sumatorialib.setCellStyle(estiloBordesTitulo);
	      HSSFCell totallib = titulo.createCell(8);
	      totallib.setCellValue("Total Ordenes Libres de Cobro");
	      totallib.setCellStyle(estiloBordesTitulo);
	      HSSFCell aceptacionlib = titulo.createCell(9);
	      aceptacionlib.setCellValue("% Aceptacion Libres de Cobro Red");
	      aceptacionlib.setCellStyle(estiloBordesTitulo);
	      int s = 5;
	      List<OrdenRedBean> datos = ReportesDao.obtenerTotalesRed(anio, campania, zona);
	      for (int i = 0; i < datos.size(); i++) {
	        HSSFRow linea = hoja3.createRow(s);
	        for (int j = 0; j < 9; j++) {
	          OrdenRedBean obj = datos.get(i);
	          if (obj != null) {
	            HSSFCell redd = linea.createCell(0);
	            redd.setCellStyle(estiloBordes);
	            redd.setCellValue(obj.getRed());
	            float o = 0.0F, p = 0.0F, q = 0.0F;
	            o = obj.getTotalOrdenes();
	            p = obj.getOrdenesDevolucion();
	            q = (o - p) / o * 100.0F;
	            HSSFCell aceptaciond = linea.createCell(1);
	            aceptaciond.setCellStyle(estiloBordes);
	            if (o != 0.0F) {
	              aceptaciond.setCellValue(df.format(q) + "%");
	            } else {
	              aceptaciond.setCellValue("");
	            } 
	            HSSFCell ordenesdevd = linea.createCell(2);
	            ordenesdevd.setCellStyle(estiloBordes);
	            ordenesdevd.setCellValue(obj.getOrdenesDevolucion());
	            HSSFCell sumatoriad = linea.createCell(3);
	            sumatoriad.setCellStyle(estiloBordes);
	            sumatoriad.setCellValue(Double.valueOf(df.format(obj.getSumatoriaRed())).doubleValue());
	            HSSFCell totald = linea.createCell(4);
	            totald.setCellStyle(estiloBordes);
	            totald.setCellValue(obj.getTotalOrdenes());
	            HSSFCell ordeneslibd = linea.createCell(6);
	            ordeneslibd.setCellStyle(estiloBordes);
	            ordeneslibd.setCellValue(obj.getOrdenesLibres());
	            HSSFCell sumatorialibd = linea.createCell(7);
	            sumatorialibd.setCellStyle(estiloBordes);
	            sumatorialibd.setCellValue(Double.valueOf(df.format(obj.getSumatoriaLibres())).doubleValue());
	            HSSFCell totallibd = linea.createCell(8);
	            totallibd.setCellStyle(estiloBordes);
	            totallibd.setCellValue(obj.getTotalLibres());
	            float x = 0.0F, y = 0.0F, z = 0.0F;
	            x = obj.getTotalLibres();
	            y = obj.getOrdenesLibres();
	            z = (x - y) / x * 100.0F;
	            HSSFCell aceptacionlibd = linea.createCell(9);
	            aceptacionlibd.setCellStyle(estiloBordes);
	            if (x != 0.0F) {
	              aceptacionlibd.setCellValue(df.format(z) + "%");
	            } else {
	              aceptacionlibd.setCellValue("");
	            } 
	          } 
	        } 
	        s++;
	      } 
	      hoja3.autoSizeColumn(0);
	      hoja3.autoSizeColumn(1);
	      hoja3.autoSizeColumn(2);
	      hoja3.autoSizeColumn(3);
	      hoja3.autoSizeColumn(4);
	      hoja3.autoSizeColumn(5);
	      hoja3.autoSizeColumn(6);
	      hoja3.autoSizeColumn(7);
	      hoja3.autoSizeColumn(8);
	      hoja3.autoSizeColumn(9);
	      return hoja3;
	    } catch (Exception e) {
	      System.out.println("Ocurrio un error al crear la hoja!");
	      loggerError.error("Exception", e);
	      return null;
	    } 
	  }
	  
	  public HSSFSheet creaHoja4Zona(ArchivoExcel2003 libro, int anio, int campania, int zona) {
	    try {
	      CellStyle estiloBordes = libro.estiloBordes();
	      CellStyle estiloBordesTitulo = libro.estiloTituloBordes();
	      CellStyle estiloBorderDer = libro.estiloBordesDerecha();
	      HSSFSheet hoja4 = libro.agregarHoja("Lista de Devoluciones");
	      hoja4.setDisplayGridlines(false);
	      HSSFRow linea1 = hoja4.createRow(0);
	      HSSFCell celda11 = linea1.createCell(2);
	      celda11.setCellStyle(libro.estiloTitulos());
	      celda11.setCellValue("LISTA DE DEVOLUCIONES");
	      HSSFRow linea2 = hoja4.createRow(1);
	      HSSFCell celda21 = linea2.createCell(0);
	      celda21.setCellStyle(estiloBordesTitulo);
	      celda21.setCellValue("ZONA");
	      HSSFCell celda22 = linea2.createCell(1);
	      celda22.setCellValue(zona);
	      celda22.setCellStyle(estiloBorderDer);
	      HSSFRow linea3 = hoja4.createRow(2);
	      HSSFCell celda31 = linea3.createCell(0);
	      celda31.setCellStyle(estiloBordesTitulo);
	      celda31.setCellValue("CAMPAÑA");
	      HSSFCell celda32 = linea3.createCell(1);
	      celda32.setCellValue("00000-0" + String.valueOf(campania));
	      celda32.setCellStyle(estiloBorderDer);
	      HSSFCell celda33 = linea3.createCell(3);
	      celda33.setCellStyle(estiloBordesTitulo);
	      celda33.setCellValue("% DEVOLUCION");
	      HSSFCell celda34 = linea3.createCell(4);
	      celda34.setCellStyle(estiloBordesTitulo);
	      celda34.setCellValue("MI VENTA DE DEVOLUCION");
	      HSSFRow linea4 = hoja4.createRow(3);
	      HSSFCell celda41 = linea4.createCell(0);
	      celda41.setCellStyle(estiloBordesTitulo);
	      celda41.setCellValue("TOTAL ORDENES");
	      HSSFCell celda42 = linea4.createCell(1);
	      celda42.setCellValue(this.cantidadTotalOrdenes);
	      celda42.setCellStyle(estiloBorderDer);
	      float a = 0.0F, b = 0.0F, c = 0.0F;
	      a = this.cantidadTotalOrdenes;
	      b = this.cantidadOrdenesDevueltas;
	      c = b / a * 100.0F;
	      DecimalFormat df = new DecimalFormat("#.##");
	      HSSFCell celda43 = linea4.createCell(3);
	      celda43.setCellStyle(estiloBordes);
	      celda43.setCellType(1);
	      celda43.setCellValue(df.format(c) + "%");
	      HSSFCell celda44 = linea4.createCell(4);
	      celda44.setCellStyle(estiloBorderDer);
	      celda44.setCellValue(Double.valueOf(df.format(this.sumaTotOrdenesDevueltas)).doubleValue());
	      HSSFRow linea5 = hoja4.createRow(4);
	      HSSFCell celda51 = linea5.createCell(0);
	      celda51.setCellStyle(estiloBordesTitulo);
	      celda51.setCellValue("ORDENES DEVUELTAS");
	      HSSFCell celda52 = linea5.createCell(1);
	      celda52.setCellValue(this.cantidadOrdenesDevueltas);
	      celda52.setCellStyle(estiloBorderDer);
	      HSSFRow linea6 = hoja4.createRow(5);
	      HSSFCell celda61 = linea6.createCell(0);
	      celda61.setCellValue("");
	      HSSFRow titulo = hoja4.createRow(6);
	      HSSFCell registro = titulo.createCell(0);
	      registro.setCellValue("REGISTRO");
	      registro.setCellStyle(estiloBordesTitulo);
	      HSSFCell nombre = titulo.createCell(1);
	      nombre.setCellValue("NOMBRE");
	      nombre.setCellStyle(estiloBordesTitulo);
	      HSSFCell direccion = titulo.createCell(2);
	      direccion.setCellValue("CALLE, NUMERO, COLONIA");
	      direccion.setCellStyle(estiloBordesTitulo);
	      HSSFCell calles = titulo.createCell(3);
	      calles.setCellValue("ENTRE CALLES");
	      calles.setCellStyle(estiloBordesTitulo);
	      HSSFCell poblacion = titulo.createCell(4);
	      poblacion.setCellValue("POBLACION");
	      poblacion.setCellStyle(estiloBordesTitulo);
	      HSSFCell valorCod = titulo.createCell(5);
	      valorCod.setCellValue("VALOR COD");
	      valorCod.setCellStyle(estiloBordesTitulo);
	      HSSFCell valorPedido = titulo.createCell(6);
	      valorPedido.setCellValue("VALOR PEDIDO");
	      valorPedido.setCellStyle(estiloBordesTitulo);
	      HSSFCell red = titulo.createCell(7);
	      red.setCellValue("RED");
	      red.setCellStyle(estiloBordesTitulo);
	      HSSFCell cajas = titulo.createCell(8);
	      cajas.setCellValue("CAJAS");
	      cajas.setCellStyle(estiloBordesTitulo);
	      HSSFCell causa = titulo.createCell(9);
	      causa.setCellValue("CAUSA DE DEVOLUCION");
	      causa.setCellStyle(estiloBordesTitulo);
	      HSSFCell balance = titulo.createCell(10);
	      balance.setCellValue("BALANCE");
	      balance.setCellStyle(estiloBordesTitulo);
	      List<OrdenBean> datos = ReportesDao.obtenerOrdenesDevueltas(anio, campania, zona);
	      int s = 7;
	      for (int k = 0; k < datos.size(); k++) {
	        HSSFRow fila = hoja4.createRow(s);
	        for (int i = 0; i < 10; i++) {
	          OrdenBean obj = datos.get(k);
	          HSSFCell registrod = fila.createCell(0);
	          registrod.setCellStyle(estiloBordes);
	          registrod.setCellValue(Utilerias.completar(8, obj.getRegistro().intValue()));
	          HSSFCell nombred = fila.createCell(1);
	          nombred.setCellStyle(estiloBordes);
	          if (obj.getNombre() == null) {
	            nombred.setCellValue("");
	          } else {
	            nombred.setCellValue(obj.getNombre().toString());
	          } 
	          HSSFCell direcciond = fila.createCell(2);
	          direcciond.setCellStyle(estiloBordes);
	          if (obj.getDireccion() == null) {
	            direcciond.setCellValue("");
	          } else {
	            direcciond.setCellValue(obj.getDireccion().toString());
	          } 
	          HSSFCell callesd = fila.createCell(3);
	          callesd.setCellStyle(estiloBordes);
	          if (obj.getCalles() == null) {
	            callesd.setCellValue("");
	          } else {
	            callesd.setCellValue(obj.getCalles().toString());
	          } 
	          HSSFCell poblaciond = fila.createCell(4);
	          poblaciond.setCellStyle(estiloBordes);
	          if (obj.getPoblacion() == null) {
	            poblaciond.setCellValue("");
	          } else {
	            poblaciond.setCellValue(obj.getPoblacion().toString());
	          } 
	          HSSFCell valorCodd = fila.createCell(5);
	          valorCodd.setCellStyle(estiloBordes);
	          valorCodd.setCellValue(Double.valueOf(df.format(obj.getValorCod())).doubleValue());
	          HSSFCell valorPedidod = fila.createCell(6);
	          valorPedidod.setCellStyle(estiloBordes);
	          valorPedidod.setCellValue(Double.valueOf(df.format(obj.getValorPedido())).doubleValue());
	          HSSFCell redd = fila.createCell(7);
	          redd.setCellStyle(estiloBordes);
	          redd.setCellValue(obj.getRed());
	          HSSFCell cajasd = fila.createCell(8);
	          cajasd.setCellStyle(estiloBordes);
	          cajasd.setCellValue(obj.getCajas().intValue());
	          HSSFCell causad = fila.createCell(9);
	          causad.setCellStyle(estiloBordes);
	          if (obj.getCausa() == null) {
	            causad.setCellValue("");
	          } else {
	            causad.setCellValue(obj.getCausa().toString());
	          } 
	          HSSFCell balanced = fila.createCell(10);
	          balanced.setCellStyle(estiloBordes);
	          balanced.setCellValue(Double.valueOf(df.format(obj.getBalance())).doubleValue());
	        } 
	        s++;
	      } 
	      int celdaFinal = this.cantidadOrdenesDevueltas + 7;
	      hoja4.setAutoFilter(CellRangeAddress.valueOf("A7:K" + celdaFinal));
	      hoja4.autoSizeColumn(0);
	      hoja4.autoSizeColumn(1);
	      hoja4.autoSizeColumn(2);
	      hoja4.autoSizeColumn(3);
	      hoja4.autoSizeColumn(4);
	      hoja4.autoSizeColumn(5);
	      hoja4.autoSizeColumn(6);
	      hoja4.autoSizeColumn(7);
	      hoja4.autoSizeColumn(8);
	      hoja4.autoSizeColumn(9);
	      hoja4.autoSizeColumn(10);
	      return hoja4;
	    } catch (Exception e) {
	      e.printStackTrace();
	      loggerError.error("Ocurrio un error al generar el libro!: " + e);
	      return null;
	    } 
	  }
	  */
}
