package com.avon.repNarFronterasAbiertas.dto;

public class FileBean {

	private String fileName;
	  
	  private int anio;
	  
	  private int campania;
	  
	  private int zona;
	  
	  public FileBean() {}
	  
	  public FileBean(String fileName, int anio, int campania, int zona) {
	    this.fileName = fileName;
	    this.anio = anio;
	    this.campania = campania;
	    this.zona = zona;
	  }
	  
	  public String getFileName() {
	    return this.fileName;
	  }
	  
	  public void setFileName(String fileName) {
	    this.fileName = fileName;
	  }
	  
	  public int getAnio() {
	    return this.anio;
	  }
	  
	  public void setAnio(int anio) {
	    this.anio = anio;
	  }
	  
	  public int getCampania() {
	    return this.campania;
	  }
	  
	  public void setCampania(int campania) {
	    this.campania = campania;
	  }
	  
	  public int getZona() {
	    return this.zona;
	  }
	  
	  public void setZona(int zona) {
	    this.zona = zona;
	  }
}
