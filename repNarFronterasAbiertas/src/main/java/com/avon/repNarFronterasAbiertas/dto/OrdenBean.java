package com.avon.repNarFronterasAbiertas.dto;

public class OrdenBean {

	private Integer registro;
	  
	  private String nombre;
	  
	  private String direccion;
	  
	  private String calles;
	  
	  private String poblacion;
	  
	  private float valorCod;
	  
	  private float valorPedido;
	  
	  private int red;
	  
	  private Integer cajas;
	  
	  private String causa;
	  
	  private float balance;
	  
	  private Integer blockedStatus;
	  
	  private String roundNum;
	  
	  private String devolRsn;
	  
	  private String delivery1St;
	  
	  public String toString() {
	    return "OrdenBean [registro=" + this.registro + ", nombre=" + this.nombre + ", direccion=" + this.direccion + ", calles=" + this.calles + ", poblacion=" + this.poblacion + ", valorCod=" + this.valorCod + ", valorPedido=" + this.valorPedido + ", red=" + this.red + ", cajas=" + this.cajas + ", causa=" + this.causa + ", balance=" + this.balance + ", blockedStatus=" + this.blockedStatus + "]";
	  }
	  
	  public String getRoundNum() {
	    return (this.roundNum == null) ? "" : this.roundNum;
	  }
	  
	  public void setRoundNum(String roundNum) {
	    this.roundNum = roundNum;
	  }
	  
	  public String getDevolRsn() {
	    return (this.devolRsn == null) ? "" : this.devolRsn;
	  }
	  
	  public void setDevolRsn(String devolRsn) {
	    this.devolRsn = devolRsn;
	  }
	  
	  public String getDelivery1St() {
	    return (this.delivery1St == null) ? "" : this.delivery1St;
	  }
	  
	  public void setDelivery1St(String delivery1St) {
	    this.delivery1St = delivery1St;
	  }
	  
	  public Integer getRegistro() {
	    return Integer.valueOf((this.registro == null) ? 0 : this.registro.intValue());
	  }
	  
	  public void setRegistro(Integer registro) {
	    this.registro = registro;
	  }
	  
	  public String getNombre() {
	    return this.nombre;
	  }
	  
	  public void setNombre(String nombre) {
	    this.nombre = nombre;
	  }
	  
	  public String getDireccion() {
	    return this.direccion;
	  }
	  
	  public void setDireccion(String direccion) {
	    this.direccion = direccion;
	  }
	  
	  public String getCalles() {
	    return this.calles;
	  }
	  
	  public void setCalles(String calles) {
	    this.calles = calles;
	  }
	  
	  public String getPoblacion() {
	    return this.poblacion;
	  }
	  
	  public void setPoblacion(String poblacion) {
	    this.poblacion = poblacion;
	  }
	  
	  public float getValorCod() {
	    return this.valorCod;
	  }
	  
	  public void setValorCod(float valorCod) {
	    this.valorCod = valorCod;
	  }
	  
	  public float getValorPedido() {
	    return this.valorPedido;
	  }
	  
	  public void setValorPedido(float valorPedido) {
	    this.valorPedido = valorPedido;
	  }
	  
	  public int getRed() {
	    return this.red;
	  }
	  
	  public void setRed(int red) {
	    this.red = red;
	  }
	  
	  public Integer getCajas() {
	    return Integer.valueOf((this.cajas == null) ? 0 : this.cajas.intValue());
	  }
	  
	  public void setCajas(Integer cajas) {
	    this.cajas = cajas;
	  }
	  
	  public String getCausa() {
	    return this.causa;
	  }
	  
	  public void setCausa(String causa) {
	    this.causa = causa;
	  }
	  
	  public float getBalance() {
	    return this.balance;
	  }
	  
	  public void setBalance(float balance) {
	    this.balance = balance;
	  }
	  
	  public Integer getBlockedStatus() {
	    return Integer.valueOf((this.blockedStatus == null) ? 0 : this.blockedStatus.intValue());
	  }
	  
	  public void setBlockedStatus(Integer blockedStatus) {
	    this.blockedStatus = blockedStatus;
	  }
}
