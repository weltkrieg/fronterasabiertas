package com.avon.repNarFronterasAbiertas.dto;

public class OrdenRedBean {

	private int red;
	  
	  private String aceptacionRed;
	  
	  private int ordenesDevolucion;
	  
	  private float sumatoriaRed;
	  
	  private int totalOrdenes;
	  
	  private int ordenesLibres;
	  
	  private float sumatoriaLibres;
	  
	  private int totalLibres;
	  
	  private String aceptacionLibres;
	  
	  public int getRed() {
	    return this.red;
	  }
	  
	  public void setRed(int red) {
	    this.red = red;
	  }
	  
	  public String getAceptacionRed() {
	    return this.aceptacionRed;
	  }
	  
	  public void setAceptacionRed(String aceptacionRed) {
	    this.aceptacionRed = aceptacionRed;
	  }
	  
	  public int getOrdenesDevolucion() {
	    return this.ordenesDevolucion;
	  }
	  
	  public void setOrdenesDevolucion(int ordenesDevolucion) {
	    this.ordenesDevolucion = ordenesDevolucion;
	  }
	  
	  public float getSumatoriaRed() {
	    return this.sumatoriaRed;
	  }
	  
	  public void setSumatoriaRed(float sumatoriaRed) {
	    this.sumatoriaRed = sumatoriaRed;
	  }
	  
	  public int getTotalOrdenes() {
	    return this.totalOrdenes;
	  }
	  
	  public void setTotalOrdenes(int totalOrdenes) {
	    this.totalOrdenes = totalOrdenes;
	  }
	  
	  public int getOrdenesLibres() {
	    return this.ordenesLibres;
	  }
	  
	  public void setOrdenesLibres(int ordenesLibres) {
	    this.ordenesLibres = ordenesLibres;
	  }
	  
	  public float getSumatoriaLibres() {
	    return this.sumatoriaLibres;
	  }
	  
	  public void setSumatoriaLibres(float sumatoriaLibres) {
	    this.sumatoriaLibres = sumatoriaLibres;
	  }
	  
	  public int getTotalLibres() {
	    return this.totalLibres;
	  }
	  
	  public void setTotalLibres(int totalLibres) {
	    this.totalLibres = totalLibres;
	  }
	  
	  public String getAceptacionLibres() {
	    return this.aceptacionLibres;
	  }
	  
	  public void setAceptacionLibres(String aceptacionLibres) {
	    this.aceptacionLibres = aceptacionLibres;
	  }
}
