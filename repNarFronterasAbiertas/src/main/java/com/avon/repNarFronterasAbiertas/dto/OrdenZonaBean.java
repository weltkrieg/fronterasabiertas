package com.avon.repNarFronterasAbiertas.dto;

public class OrdenZonaBean {

	private int registro;
	  
	  private int red;
	  
	  private float valorCod;
	  
	  public int getRegistro() {
	    return this.registro;
	  }
	  
	  public void setRegistro(int registro) {
	    this.registro = registro;
	  }
	  
	  public int getRed() {
	    return this.red;
	  }
	  
	  public void setRed(int red) {
	    this.red = red;
	  }
	  
	  public float getValorCod() {
	    return this.valorCod;
	  }
	  
	  public void setValorCod(float valorCod) {
	    this.valorCod = valorCod;
	  }
}
