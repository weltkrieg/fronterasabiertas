package com.avon.repNarFronterasAbiertas.dto;

public class ZonasBean {

	private int zona;
	  
	private int anio;
	  
	public int getZona() {
	  return this.zona;
	}
	  
	public void setZona(int zona) {
	    this.zona = zona;
	}
	  
	public int getAnio() {
	   return this.anio;
	}
	  
	public void setAnio(int anio) {
	    this.anio = anio;
	}
}
